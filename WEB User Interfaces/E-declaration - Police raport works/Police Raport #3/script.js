var app = angular.module("politseiApp", []);
var CURRENT_YEAR = new Date().getFullYear();

function validDOB(date)
{
	return getDate(date) < new Date();
}

function checkDates(pos, loss)
{
	var posValid = false;
	var lossValid = false;
	if (loss != "") //If we have loss value
	{
		lossValid = getDate(loss) <= getDate();
	}
	if (pos != "" && loss != "") //If we have both pos and loss value
	{
		posValid = getDate(pos) <= getDate(loss);
	}
	return {"posValid":posValid, "lossValid":lossValid};
}

function validPersonalData(values)
{
	var idValid = validID(values.id);
	var fnameValid = validString(values.fname);
	var snameValid = validString(values.sname);
	var dobValid = validString(values.dob);
	if (idValid)
	{
		fnameValid = true;
		snameValid = true;
		dobValid = true;
	}
	if (fnameValid && snameValid)
	{
		idValid = true;
	}
	return {"idValid":idValid, "fnameValid":fnameValid, "snameValid":snameValid, "dobValid":dobValid};
}

function checkPersonalDataDirective(form, obj)
{
	var id = obj.viewId;
	var fname = obj.viewFname;
	var sname = obj.viewSname;
	var dob = obj.viewDob;
	var values = {"id":id, "fname":fname, "sname":sname, "dob":dob};
	var valid = validPersonalData(values);
	var idValid = valid.idValid;
	var fnameValid = valid.fnameValid;
	var snameValid = valid.snameValid;
	var dobValid = valid.dobValid;
	form.id.$setValidity("checkIdWitness", idValid);
	form.fname.$setValidity("checkFnameWitness", fnameValid);
	form.sname.$setValidity("checkSnameWitness", snameValid);
	form.dob.$setValidity("checkDobWitness", dobValid);
	return valid;
}

function checkPersonalData(form)
{
	var id = form.id;
	var fname = form.fname;
	var sname = form.sname;
	var dob = form.dob;
	var valid = validPersonalData({"id":id, "fname":fname, "sname":sname, "dob":dob});
	var idValid = valid.idValid;
	var fnameValid = valid.fnameValid;
	var snameValid = valid.snameValid;
	var dobValid = valid.dobValid;
	id.$setValidity("validID", idValid);
	fname.$setValidity("validFName", fnameValid);
	sname.$setValidity("validSName", snameValid);
	dob.$setValidity("required", dobValid);
}

function getDate(v = "")
{
	if (v == "")
	{
		return new Date();
	}
	var vals = /(\d{2})\.(\d{2})\.(\d{4})/;
	if (!vals.test(v))
	{
		return new Date();
	}
	var time = vals.exec(v);
	return new Date(Number(time[3]), Number(time[2])-1, Number(time[1]));
}

function validID(id)
{
	var test = /^\d{11}$/;
	return test.test(id);
}

function validString(name)
{
	return name != undefined && name.length != undefined && name.length > 0;
}

function checkPhone(phone)
{
	var test = /^\+?[\d\(\)\-]+$/;
	var val = phone.$modelValue;
	phone.$setValidity("validPhone", test.test(val));
}

function checkZipcode(zipcode)
{
	zipcode.$setValidity("validZipcode", true);
	var val = zipcode.$modelValue;
	if (val == undefined || val.length == undefined || val == "")
	{
		zipcode.$setValidity("validZipcode", true);
		return;
	}
	if (val.toString().length != 5)
	{
		zipcode.$setValidity("validZipcode", false);
	}
}

function form1Valid($scope)
{
	return $scope.eventForm.$valid;
}

function form2Valid($scope)
{
	return $scope.itemForm.$valid;
}

function form3Valid($scope)
{
	return $scope.offenderForm.$valid;
}

function form4Valid($scope)
{
	return $scope.witnessForm.$valid;
}

function form5Valid($scope)
{
	return $scope.applicantForm.$valid;
}

function formValid($scope)
{
	n = $scope.activeTab;
	if (n < 1 || n > 5)
	{
		return false;
	}
	n = n-1;

	checks = 
		[
		form1Valid,
		form2Valid,
		form3Valid,
		form4Valid,
		form5Valid
		];
	return checks[n]($scope);
}

app.directive('lossDiscoveryItem', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ctrl) 
		{
			ctrl.$validators.lossDiscovery = function(modelValue, viewValue)
			{
				if (ctrl.$isEmpty(modelValue))
				{
					return false;
				}
				var pos = scope.obj.lastView;
				var loss = modelValue;
				var valid = checkDates(pos, loss);
				scope.itemForm.lastInPosessionDate.$setValidity("lastInPosession", valid.posValid);
				return valid.lossValid;
			};
		}
	};
});

app.directive('lastInPosessionItem', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ctrl) 
		{
			ctrl.$validators.lastInPosession = function(modelValue, viewValue)
			{
				if (ctrl.$isEmpty(modelValue))
				{
					return false;
				}
				var pos = modelValue;
				var loss = scope.obj.lossDiscoveryDate;
				scope.obj.lastView = pos;
				return checkDates(pos, loss).posValid;
			};
		}
	};
});

app.directive('checkIdWitness', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ctrl) 
		{
			ctrl.$validators.checkIdWitness = function(modelValue, viewValue)
			{
				scope.obj.viewId = modelValue;
				var valid = checkPersonalDataDirective(scope.witnessForm, scope.obj);
				return valid.idValid;
			};
		}
	};
});

app.directive('checkFnameWitness', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ctrl) 
		{
			ctrl.$validators.checkFnameWitness = function(modelValue, viewValue)
			{
				scope.obj.viewFname = modelValue;
				var valid = checkPersonalDataDirective(scope.witnessForm, scope.obj);
				return valid.fnameValid;
			};
		}
	};
});

app.directive('checkSnameWitness', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ctrl) 
		{
			ctrl.$validators.checkSnameWitness = function(modelValue, viewValue)
			{
				scope.obj.viewSname = modelValue;
				var valid = checkPersonalDataDirective(scope.witnessForm, scope.obj);
				return valid.snameValid;
			};
		}
	};
});

app.directive('checkDobWitness', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ctrl) 
		{
			ctrl.$validators.checkDobWitness = function(modelValue, viewValue)
			{
				scope.obj.viewDob = modelValue;
				var valid = checkPersonalDataDirective(scope.witnessForm, scope.obj);
				return valid.dobValid;
			};
		}
	};
});

app.controller("politseiController", ['$scope', function($scope) {

	$scope.nextButtonText = "Edasi";

	$scope.activeTab = 1;
	$scope.maxTab = 1;
	$scope.defCountry = "NAN";

	$scope.eventCountry = "NAN";
	$scope.witnessCountry = "NAN";
	$scope.offenderCountry = "NAN";
	$scope.applicantCountry = "NAN";

	$scope.items = [{"ind" : 0, "lastInPosessionDate":"", "lossDiscoveryDate":"", "lastView": ""}];
	$scope.offenders = [{"ind" : 0}];
	$scope.witnesses = [{"ind" : 0, "dob":"", "viewDob":"", "id":"","viewId":"", "fname":"", "viewFname":"", "sname":"", "viewSname":""}];

	$scope.applicantPrefecture = "Üldine kontakt";
	$scope.preferredContactMethod = "telefoni teel";
    
	$scope.getFormByIndex = function(n) {
		switch(n) {
			case 1:
				return $scope.eventForm;
			case 2:
				return $scope.itemForm;
			case 3:
				return $scope.offenderForm;
			case 4:
				return $scope.witnessForm;
			case 5:
				return $scope.applicantForm;
		}
	};

	$scope.addNextItem = function()
	{
		var arr = $scope.items;
		arr.push({"ind": arr.length, "lastInPosessionDate": "", "lossDiscoveryDate": "", "lastView": ""});
	}

	$scope.removeLastItem = function()
	{
		var arr = $scope.items;
		if (arr.length > 0)
		{
			arr.splice(arr.length-1,1);
		}
	}

	$scope.addOffender = function()
	{
		var arr = $scope.offenders;
		arr.push({"ind": arr.length});
	}

	$scope.removeOffender = function()
	{
		var arr = $scope.offenders;
		if (arr.length > 0)
		{
			arr.splice(arr.length-1,1);
		}
	}

	$scope.addWitness = function()
	{
		var arr = $scope.witnesses;
		arr.push({"ind" : arr.length, "dob":"", "viewDob":"", "id":"","viewId":"", "fname":"", "viewFname":"", "sname":"", "viewSname":""});
	}

	$scope.removeWitness = function()
	{
		var arr = $scope.witnesses;
		if (arr.length > 0)
		{
			arr.splice(arr.length-1,1);
		}
	}

	$scope.clickBreadcrumb = function(n) {
		if (n > $scope.maxTab)
		{
			return;
		}
		$scope.activeTab = n;
	};

	$scope.updateSubmissionText = function() {
		if($scope.activeTab == 5) {
			console.log($scope.applicantForm.usesDigitalSignature.$modelValue);
			if($scope.applicantForm.usesDigitalSignature.$modelValue == true) {
				$scope.nextButtonText = "Kinnita ja allkirjasta";
			} else {
				$scope.nextButtonText = "Kinnita allkirjastamata";
			}
		} else {
			$scope.nextButtonText = "Edasi";
		}
	};

	$scope.clickBack = function() {
		if($scope.activeTab > 1) $scope.activeTab--;
		$scope.updateSubmissionText();
	};

	$scope.clickNext = function() {
		if (!formValid($scope))
		{
			$scope.getFormByIndex($scope.activeTab).$setDirty();
			return;
		}
		if($scope.activeTab < 5) $scope.activeTab++;
		if ($scope.maxTab < $scope.activeTab)
		{
			$scope.maxTab = $scope.activeTab;
		}
		$scope.updateSubmissionText();
	};

	$scope.$watch("applicantForm.usesDigitalSignature", $scope.updateSubmissionText);

	//$("#datepicker-main-event").datepicker().on("changeDate", function()
	//{
	//	var val = $("#datepicker-main-event-value").val($(this).datepicker("getDate"));
	//	console.log($scope.eventForm.date);
	//	console.log(val);
	//});

	$scope.$watch("applicantForm", function() {
		$scope.applicantForm.id.$setValidity("validID", false);
		$scope.applicantForm.fname.$setValidity("validFName", false);
		$scope.applicantForm.sname.$setValidity("validSName", false);
	});

	$scope.$watch("applicant.dob", function() {
		var dob = $scope.applicantForm.dob;
		var check = $scope.applicantForm.usesDigitalSignature.$modelValue;
		if (!check)
		{
			dob.$setValidity("required", validDOB(dob.$modelValue));
			return;
		}
		dob.$setValidity("required", true);
	});

	$scope.$watch("applicant.usesDigitalSignature", function() {
		var check = $scope.applicantForm.usesDigitalSignature.$modelValue;
		if (check == undefined)
		{
			return;
		}
		if (check)
		{
			$scope.applicantForm.id.$setValidity("validID", true);
			$scope.applicantForm.fname.$setValidity("validFName", true);
			$scope.applicantForm.sname.$setValidity("validSName", true);
			$scope.applicantForm.dob.$setValidity("required", true);
		}
		else
		{
			checkPersonalData($scope.applicantForm);
		}
	});

	$scope.$watch("applicant.phone", function() {
		checkPhone($scope.applicantForm.phone);
	});

	$scope.$watch("event.time", function() {
		var test = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
		var val = $scope.eventForm.time.$modelValue;
		$scope.eventForm.time.$setValidity("validTime", test.test(val));
	});

	$scope.$watch("event.zipcode", function() {
		checkZipcode($scope.eventForm.zipcode);
	});

	$scope.$watch("applicant.zipcode", function() {
		checkZipcode($scope.applicantForm.zipcode);
	});

	$scope.$watch("applicant.companyRegNumber", function() {
		var test = /^[\w\d]*$/;
		var val = $scope.applicantForm.companyRegNumber.$modelValue;
		var valid = test.test(val);
		var check = $scope.applicantForm.usesDigitalSignature.$modelValue;
		if (!validString(val))
		{
			$scope.applicantForm.companyRegNumber.$setValidity("validRegNumber", true);
			if (!check)
			{
				$scope.applicantForm.id.$setValidity("validID", validID($scope.applicantForm.id.$modelValue));
				$scope.applicantForm.fname.$setValidity("validFName", validString($scope.applicantForm.fname.$modelValue));
				$scope.applicantForm.sname.$setValidity("validSName", validString($scope.applicantForm.sname.$modelValue));
				$scope.applicantForm.dob.$setValidity("required", validDOB($scope.applicantForm.address.$modelValue));
			}
			return;
		}
		$scope.applicantForm.companyRegNumber.$setValidity("validRegNumber", valid);
		if (valid)
		{
			$scope.applicantForm.id.$setValidity("validID", true);
			$scope.applicantForm.fname.$setValidity("validFName", true);
			$scope.applicantForm.sname.$setValidity("validSName", true);
			$scope.applicantForm.dob.$setValidity("required", true);
		}
		else
		{
			if (!check)
			{
				checkPersonalData($scope.applicantForm);
			}
		}
	});

	$scope.$watch("applicant.id", function() {
		var check = $scope.applicantForm.usesDigitalSignature.$modelValue;
		if (!check)
		{
			checkPersonalData($scope.applicantForm);
			return;
		}
		$scope.applicantForm.id.$setValidity("validID", true);
	});

	$scope.$watch("applicant.fname", function() {
		var check = $scope.applicantForm.usesDigitalSignature.$modelValue;
		if (!check)
		{
			checkPersonalData($scope.applicantForm);
			return;
		}
		$scope.applicantForm.fname.$setValidity("validFName", true);
	});

	$scope.$watch("applicant.sname", function() {
		var check = $scope.applicantForm.usesDigitalSignature.$modelValue;
		if (!check)
		{
			checkPersonalData($scope.applicantForm);
			return;
		}
		$scope.applicantForm.sname.$setValidity("validSName", true);
	});

	/* Initializing country selection */
	$scope.data = 
	{
		model: null,
		availableCountries:
			[
			{label: "Eesti Vabariik", value: "EST"},
			{label: "---", value: "NAN"},
			{label: "Afganistan", value: "AFG"},
			{label: "Ahvenamaa", value: "ALA"},
			{label: "Albaania", value: "ALB"},
			{label: "Alžeeria", value: "DZA"},
			{label: "Ameerika Samoa", value: "ASM"},
			{label: "Ameerika Ühendriigid", value: "USA"},
			{label: "Andorra", value: "AND"},
			{label: "Angola", value: "AGO"},
			{label: "Anguilla", value: "AIA"},
			{label: "Antarktis", value: "ATA"},
			{label: "Antigua ja Barbuda", value: "ATG"},
			{label: "Araabia Ühendemiraadid", value: "ARE"},
			{label: "Argentina", value: "ARG"},
			{label: "Armeenia", value: "ARM"},
			{label: "Aruba", value: "ABW"},
			{label: "Austraalia", value: "AUS"},
			{label: "Austria", value: "AUT"},
			{label: "Aserbaidžaan", value: "AZE"},
			{label: "Bahama", value: "BHS"},
			{label: "Bahrein", value: "BHR"},
			{label: "Bangladesh", value: "BGD"},
			{label: "Barbados", value: "BRB"},
			{label: "Belau", value: "PLW"},
			{label: "Belgia", value: "BEL"},
			{label: "Belize", value: "BLZ"},
			{label: "Benin", value: "BEN"},
			{label: "Bermuda", value: "BMU"},
			{label: "Bhutan", value: "BTN"},
			{label: "Birma", value: "MMR"},
			{label: "Boliivia", value: "BOL"},
			{label: "Bosnia ja Hertsegoviina", value: "BIH"},
			{label: "Botswana", value: "BWA"},
			{label: "Bouvet", value: "BVT"},
			{label: "Brasiilia", value: "BRA"},
			{label: "Briti India ookeani ala", value: "IOT"},
			{label: "Briti Neitsisaared", value: "VGB"},
			{label: "Brunei", value: "BRN"},
			{label: "Bulgaaria", value: "BGR"},
			{label: "Burkina Faso", value: "BFA"},
			{label: "Burundi", value: "BDI"},
			{label: "Colombia", value: "COL"},
			{label: "Cooki saared", value: "COK"},
			{label: "Costa Rica", value: "CRI"},
			{label: "Djibouti", value: "DJI"},
			{label: "Dominica", value: "DMA"},
			{label: "Dominikaani Vabariik", value: "DOM"},
			{label: "Ecuador", value: "ECU"},
			{label: "Egiptus", value: "EGY"},
			{label: "Ekvatoriaal-Guinea", value: "GNQ"},
			{label: "El Salvador", value: "SLV"},
			{label: "Elevandiluurannik", value: "CIV"},
			{label: "Eritrea", value: "ERI"},
			{label: "Etioopia", value: "ETH"},
			{label: "Falklandi saared", value: "FLK"},
			{label: "Fidži", value: "FJI"},
			{label: "Fääri saared", value: "FRO"},
			{label: "Gabon", value: "GAB"},
			{label: "Gambia", value: "GMB"},
			{label: "Ghana", value: "GHA"},
			{label: "Gibraltar", value: "GIB"},
			{label: "Grenada", value: "GRD"},
			{label: "Gruusia", value: "GEO"},
			{label: "Gröönimaa", value: "GRL"},
			{label: "Guadeloupe", value: "GLP"},
			{label: "Guam", value: "GUM"},
			{label: "Guatemala", value: "GTM"},
			{label: "Guernsey", value: "GGY"},
			{label: "Guinea", value: "GIN"},
			{label: "Guinea-Bissau", value: "GNB"},
			{label: "Guyana", value: "GUY"},
			{label: "Filipiinid", value: "PHL"},
			{label: "Haiti", value: "HTI"},
			{label: "Heard ja McDonald", value: "HMD"},
			{label: "Hiina", value: "CHN"},
			{label: "Hispaania", value: "ESP"},
			{label: "Holland", value: "NLD"},
			{label: "Hollandi Antillid", value: "ANT"},
			{label: "Honduras", value: "HND"},
			{label: "Hongkong", value: "HKG"},
			{label: "Horvaatia", value: "HRV"},
			{label: "Iirimaa", value: "IRL"},
			{label: "Iisrael", value: "ISR"},
			{label: "Ida-Timor", value: "TLS"},
			{label: "India", value: "IND"},
			{label: "Indoneesia", value: "IDN"},
			{label: "Island", value: "ISL"},
			{label: "Iraak", value: "IRQ"},
			{label: "Iraan", value: "IRN"},
			{label: "Itaalia", value: "ITA"},
			{label: "Jaapan", value: "JPN"},
			{label: "Jamaica", value: "JAM"},
			{label: "Jeemen", value: "YEM"},
			{label: "Jersey", value: "JEY"},
			{label: "Jordaania", value: "JOR"},
			{label: "Jõulusaar", value: "CXR"},
			{label: "Kaimanisaared", value: "CYM"},
			{label: "Kambodža", value: "KHM"},
			{label: "Kamerun", value: "CMR"},
			{label: "Kanada", value: "CAN"},
			{label: "Kasahstan", value: "KAZ"},
			{label: "Katar", value: "QAT"},
			{label: "Keenia", value: "KEN"},
			{label: "Kesk-Aafrika Vabariik", value: "CAF"},
			{label: "Kiribati", value: "KIR"},
			{label: "Komoorid", value: "COM"},
			{label: "Kongo Demokraatlik Vabariik", value: "COD"},
			{label: "Kongo Vabariik", value: "COG"},
			{label: "Kookossaared", value: "CCK"},
			{label: "Kreeka", value: "GRC"},
			{label: "Kuuba", value: "CUB"},
			{label: "Kuveit", value: "KWT"},
			{label: "Küpros", value: "CYP"},
			{label: "Kõrgõzstan", value: "KGZ"},
			{label: "Laos", value: "LAO"},
			{label: "Leedu", value: "LTU"},
			{label: "Lesotho", value: "LSO"},
			{label: "Liibanon", value: "LBN"},
			{label: "Libeeria", value: "LBR"},
			{label: "Liechtenstein", value: "LIE"},
			{label: "Liibüa", value: "LBY"},
			{label: "Luksemburg", value: "LUX"},
			{label: "Lõuna-Aafrika Vabariik", value: "ZAF"},
			{label: "Lõuna-Georgia ja Lõuna-Sandwichi saared", value: "SGS"},
			{label: "Lõuna-Korea", value: "KOR"},
			{label: "Lääne-Sahara", value: "ESH"},
			{label: "Läti", value: "LVA"},
			{label: "Macau", value: "MAC"},
			{label: "Madagaskar", value: "MDG"},
			{label: "Makedoonia", value: "MKD"},
			{label: "Malaisia", value: "MYS"},
			{label: "Malawi", value: "MWI"},
			{label: "Maldiivid", value: "MDV"},
			{label: "Mali", value: "MLI"},
			{label: "Malta", value: "MLT"},
			{label: "Man", value: "IMN"},
			{label: "Maroko", value: "MAR"},
			{label: "Marshalli Saared", value: "MHL"},
			{label: "Martinique", value: "MTQ"},
			{label: "Mauritaania", value: "MRT"},
			{label: "Mauritius", value: "MUS"},
			{label: "Mayotte", value: "MYT"},
			{label: "Mehhiko", value: "MEX"},
			{label: "Mikroneesia Liiduriigid", value: "FSM"},
			{label: "Moldova", value: "MDA"},
			{label: "Monaco", value: "MCO"},
			{label: "Mongoolia", value: "MNG"},
			{label: "Montenegro", value: "MNE"},
			{label: "Montserrat", value: "MSR"},
			{label: "Mosambiik", value: "MOZ"},
			{label: "Namiibia", value: "NAM"},
			{label: "Nauru", value: "NRU"},
			{label: "Nepal", value: "NPL"},
			{label: "Nicaragua", value: "NIC"},
			{label: "Nigeeria", value: "NGA"},
			{label: "Niger", value: "NER"},
			{label: "Niue", value: "NIU"},
			{label: "Norfolk", value: "NFK"},
			{label: "Norra", value: "NOR"},
			{label: "Omaan", value: "OMN"},
			{label: "Paapua Uus-Guinea", value: "PNG"},
			{label: "Pakistan", value: "PAK"},
			{label: "Palestiina", value: "PSE"},
			{label: "Panama", value: "PAN"},
			{label: "Paraguay", value: "PRY"},
			{label: "Peruu", value: "PER"},
			{label: "Pitcairn", value: "PCN"},
			{label: "Poola", value: "POL"},
			{label: "Portugal", value: "PRT"},
			{label: "Prantsuse Guajaana", value: "GUF"},
			{label: "Prantsuse Lõunaalad", value: "ATF"},
			{label: "Prantsuse Polüneesia", value: "PYF"},
			{label: "Prantsusmaa", value: "FRA"},
			{label: "Puerto Rico", value: "PRI"},
			{label: "Põhja-Korea", value: "PRK"},
			{label: "Põhja-Mariaanid", value: "MNP"},
			{label: "Réunion", value: "REU"},
			{label: "Roheneemesaared", value: "CPV"},
			{label: "Rootsi", value: "SWE"},
			{label: "Rumeenia", value: "ROU"},
			{label: "Rwanda", value: "RWA"},
			{label: "Saalomoni Saared", value: "SLB"},
			{label: "Saint-Barthélemy", value: "BLM"},
			{label: "Saint Helena", value: "SHN"},
			{label: "Saint Kitts ja Nevis", value: "KNA"},
			{label: "Saint Lucia", value: "LCA"},
			{label: "Saint-Martin", value: "MAF"},
			{label: "Saint-Pierre ja Miquelon", value: "SPM"},
			{label: "Saint Vincent ja Grenadiinid", value: "VCT"},
			{label: "Saksamaa", value: "DEU"},
			{label: "Sambia", value: "ZMB"},
			{label: "Samoa", value: "WSM"},
			{label: "San Marino", value: "SMR"},
			{label: "São Tomé ja Príncipe", value: "STP"},
			{label: "Saudi Araabia", value: "SAU"},
			{label: "Seišellid", value: "SYC"},
			{label: "Senegal", value: "SEN"},
			{label: "Serbia", value: "SRB"},
			{label: "Sierra Leone", value: "SLE"},
			{label: "Singapur", value: "SGP"},
			{label: "Slovakkia", value: "SVK"},
			{label: "Sloveenia", value: "SVN"},
			{label: "Somaalia", value: "SOM"},
			{label: "Soome", value: "FIN"},
			{label: "Sri Lanka", value: "LKA"},
			{label: "Sudaan", value: "SDN"},
			{label: "Suriname", value: "SUR"},
			{label: "Suurbritannia", value: "GBR"},
			{label: "Svaasimaa", value: "SWZ"},
			{label: "Svalbard ja Jan Mayen", value: "SJM"},
			{label: "Süüria", value: "SYR"},
			{label: "Šveits", value: "CHE"},
			{label: "Zimbabwe", value: "ZWE"},
			{label: "Taani", value: "DNK"},
			{label: "Tadžikistan", value: "TJK"},
			{label: "Tai", value: "THA"},
			{label: "Taiwan", value: "TWN"},
			{label: "Tansaania", value: "TZA"},
			{label: "Togo", value: "TGO"},
			{label: "Tokelau", value: "TKL"},
			{label: "Tonga", value: "TON"},
			{label: "Trinidad ja Tobago", value: "TTO"},
			{label: "Tšaad", value: "TCD"},
			{label: "Tšehhi", value: "CZE"},
			{label: "Tšiili", value: "CHL"},
			{label: "Tuneesia", value: "TUN"},
			{label: "Türgi", value: "TUR"},
			{label: "Türkmenistan", value: "TKM"},
			{label: "Turks ja Caicos", value: "TCA"},
			{label: "Tuvalu", value: "TUV"},
			{label: "Uganda", value: "UGA"},
			{label: "Ukraina", value: "UKR"},
			{label: "Ungari", value: "HUN"},
			{label: "Uruguay", value: "URY"},
			{label: "USA Neitsisaared", value: "VIR"},
			{label: "Usbekistan", value: "UZB"},
			{label: "Uus-Kaledoonia", value: "NCL"},
			{label: "Uus-Meremaa", value: "NZL"},
			{label: "Valgevene", value: "BLR"},
			{label: "Vanuatu", value: "VUT"},
			{label: "Vatikan", value: "VAT"},
			{label: "Venemaa Föderatsioon", value: "RUS"},
			{label: "Venezuela", value: "VEN"},
			{label: "Vietnam", value: "VNM"},
			{label: "Wallis ja Futuna", value: "WLF"},
			{label: "Ühendriikide hajasaared", value: "UMI"}
		]
	};

}]);

$(document).ready(function() {

	/* Initializing fancy elements */

	$("#datepicker-main-event").datepicker({
		language: "et",
		todayHighlight: true,
		format: "dd.mm.yyyy",
		beforeShowDay: function(date) {
			return date < getDate();
		}
	}).on("changeDate", function(e) {
		$("#datepicker-main-event-value").val($(this).datepicker("getDate"));
	}).datepicker("setDate", new Date());

    window.setInterval(function() {
        $(".datepicker-compact:not([data-initialized='yes'])").datepicker({
            language: "et",
            todayHighlight: true,
            format: "dd.mm.yyyy",
            beforeShowDay: function(date) {
                return date < getDate();
            }
        }).data("initialized", "yes");
    }, 250);

	$(".clockpicker").clockpicker({
		placement: "top",
		align: "right",
		donetext: "OK"
	});

	/* Shims */

	// Disable scroll when focused on a number input.
	$('form').on('focus', 'input[type=number]', function(e) {
		$(this).on('wheel', function(e) {
			e.preventDefault();
		});
	})    // Restore scroll on number inputs.
	.on('blur', 'input[type=number]', function(e) {
		$(this).off('wheel');
	})    // Disable up and down keys.
	.on('keydown', 'input[type=number]', function(e) {
		if ( e.which == 38 || e.which == 40 )
			e.preventDefault();
	});

});

