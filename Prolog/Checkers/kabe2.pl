:- module(kabe142676, [kabe142676/3]).

use_if_not_zero(0, _).
use_if_not_zero(X, X).
 
leia_suund(1,1):- !.
leia_suund(2,-1).
 
is_my_status(C, C).
is_my_status(1, 10).
is_my_status(2, 20).

get_new_status(1, 8, 10) :-
    write(' (saab tammiks) ').
get_new_status(2, 1, 20) :-
    write(' (saab tammiks) ').
get_new_status(T, _, T).
 
tamm(10).
tamm(20).

  
kabe142676(MyColor):-
    ruut(X, Y, Status),
    is_my_status(MyColor, Status),
    nl, write([Status, 'Nupp ', ruudul, X, Y]),
    leia_suund(MyColor, Suund),
    votmine(X, Y, Suund, _, _, MyColor),
    !.
kabe142676(MyColor):-
    ruut(X, Y, Status),
    is_my_status(MyColor, Status),
    nl, write([Status, 'Nupp ', ruudul, X,Y]),
    leia_suund(MyColor, Suund),
    kaimine(X, Y, Suund, _, _),
    !.
kabe142676(_).
 
kabe142676(MyColor, Xin, Yin):-
    use_if_not_zero(Xin, X),
    use_if_not_zero(Yin, Y),
    ruut(X, Y, Status),
    is_my_status(MyColor, Status),
    nl, write([Status, 'Nupp ', ruudul, X, Y]),
    leia_suund(MyColor, Suund),
    votmine(X, Y, Suund, _, _, MyColor),
    !.
kabe142676(MyColor, Xin, Yin):-
    use_if_not_zero(Xin, X),
    use_if_not_zero(Yin, Y),
    ruut(X, Y, Status),
    is_my_status(MyColor, Status),
    nl, write([Status, 'Nupp ', ruudul, X,Y]),
    leia_suund(MyColor, Suund),
    kaimine(X, Y, Suund, _, _),
    !.
kabe142676(_, _, _).
%--------------------------------
votmine(X,Y,Suund,X1,Y1, MyColor):-
    kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor), 
    vota(X,Y,Suund,X1,Y1,X2,Y2), !,
    write([' votab ', X1,Y1, ' ja liigub ', X2, Y2]).
    %fail.
%--------
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine edasi paremale
    X1 is X + Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, Status),
    not(is_my_status(MyColor, Status)),
    Status =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine edasi vasakule
    X1 is X + Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, Status),
    not(is_my_status(MyColor, Status)),
    Status =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine tagasi paremale
    X1 is X + Suund * -1,
    Y1 is Y + 1,
    ruut(X1,Y1, Status),
    not(is_my_status(MyColor, Status)),
    Status =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor):-  % Votmine tagasi vasakule
    X1 is X + Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, Status),
    not(is_my_status(MyColor, Status)),
    Status =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).
 
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2, MyColor) :- % tamm
    ruut(X, Y, Status),
    tamm(Status),
 
    % find both diagonals
    %repeat,
    get_diagonals(X, Y, X1, Y1),
 
    ruut(X1,Y1, TargetStatus),
    not(is_my_status(MyColor, TargetStatus)),
    TargetStatus =\= 0,
    kas_saab_votta_nuppu(X,Y,Suund,X1,Y1,X2,Y2, MyColor).
    
kas_saab_votta_nuppu(X,Y,_,X1,Y1,X2,Y2,_) :-
    % all squares empty between them
    nothing_between(X, Y, X1, Y1),
    % empty space directly after target
    % find X2, Y2.
    space_behind_target(X, Y, X1, Y1, X2, Y2),
    % done
    write(' (tamm saab votta) ').
 
get_diagonals(X, Y, X1, Y1) :-
    between(1, 16, Y1t),
    (Y1t =< 8 ->
    Y1 is Y1t,
    X1 is Y1 + X - Y;
    Y1 is Y1t - 8,
    X1 is X + Y - Y1),
    X1 >= 1,
    X1 =< 8.
get_diagonals(X, Y, -1, -1).
 
nothing_between(X, Y, X1, Y1) :-
    get_diagonals(X, Y, Xi, Yi),
    (Xi >= 0 ->
        (should_be_empty(X, Y, X1, Y1, Xi, Yi) ->
            (not(ruut(Xi, Yi, 0)) -> !, write('should be filled but is not empty'), fail; 
            write('should be filled and is empty'), nl, fail);
        write('no matter'), nl, fail);
    write('end of get_diagonals func'), nl),
    write('end of nothing_between func'), nl.
 
should_be_empty(X, Y, X1, Y1, Xi, Yi) :-
    distance(X, Y, Xi, Yi, DistanceToPoint),
    distance(X, Y, X1, Y1, DistanceToTarget),
    DistanceToPoint > 0,
    DistanceToPoint < DistanceToTarget,
    DifX1 is X1 - X,
    DifXi is Xi - X,
    sign(DifX1) =:= sign(DifXi),
    DifY1 is Y1 - Y,
    DifYi is Yi - Y,
    sign(DifY1) =:= sign(DifYi).
 
% jump to next empty place
space_behind_target(X, Y, X1, Y1, Xt, Yt) :-
    DifX is X1 - X,
    DifY is Y1 - Y,
    SignX is sign(DifX),
    SignY is sign(DifY),
    Xt is X1 + SignX,
    Yt is Y1 + SignY,
    ruut(Xt, Yt, 0).
 
 
%--------------------------------
kaimine(X,Y,Suund,X1,Y1):-
    kas_naaber_vaba(X,Y,Suund,X1,Y1),
    tee_kaik(X,Y,X1,Y1), !,
    write([' kaib ', X1,Y1]).
%   fail.
%kaimine(_,_,_,_,_).
 
kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).
kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y -1,
    ruut(X1,Y1, 0), write(' voi ').
 
kas_naaber_vaba(X,Y,X1,Y1):-
    ruut(X,Y, Status),
    assert(ruut1(X1,Y1, Status)),!.
 
kas_naaber_vaba(X,Y,Suund,X1,Y1) :- % tamm can move backwards!!
    ruut(X, Y, Status),
    tamm(Status),
    X1 is X - Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).
 
kas_naaber_vaba(X,Y,Suund,X1,Y1) :- % tamm can move backwards!!
    ruut(X, Y, Status),
    tamm(Status),
    X1 is X - Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, 0).
 
distance(X1, Y1, X2, Y2, Distance) :-
    DifX is X2 - X1,
    DifY is Y2 - Y1,
    Distance is max(DifX, DifY).
    
tee_kaik(X, Y, X1, Y1) :-
    retract(ruut(X, Y, Status)),
    retract(ruut(X1, Y1, _)),
    get_new_status(Status, X1, NewStatus),
    assert(ruut(X1, Y1, NewStatus)),
    assert(ruut(X, Y, 0)).
 
vota(X, Y, Suund, X1, Y1, X2, Y2) :-
    retract(ruut(X, Y, Status)),
    assert(ruut(X, Y, 0)),
    retract(ruut(X2, Y2, _)),
    get_new_status(Status, X2, NewStatus),
    assert(ruut(X2, Y2, NewStatus)),
    retract(ruut(X1, Y1, _)),
    assert(ruut(X1, Y1, 0)).