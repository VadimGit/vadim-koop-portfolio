:- module(kabe, [priit/3]).

:- dynamic myColor/1, checked/2.

/*
:- dynamic ruut/3.
start:-
    retractall(ruut(_,_,_)),
    set_up_board.
*/
priit(MyColor, X, Y):-
    retractall(myColor(_)),
    assert(myColor(MyColor)),
    (X is 0 , Y is 0,
    kaigu_variandid(_, _), !);
    (kas_saab_votta(X, Y, X1, Y1, X2, Y2), 
    vota(X, Y, X1, Y1, X2, Y2)), !.
priit(_,_,_).


%-------------------------------- KÄIGU OTSIMINE

kaigu_variandid(X,Y):-
    myColor(MyColor),
    ruut(X, Y, Color),
    (Color is MyColor; Color is MyColor * 10),
    kas_saab_votta(X, Y, X1, Y1, X2, Y2), 
    vota(X, Y, X1, Y1, X2, Y2).


kaigu_variandid(X,Y):-
    myColor(MyColor),
    ruut(X, Y, Color),
    (Color is MyColor; Color is MyColor * 10),
    kas_saab_kaia(X, Y, X1, Y1), 
    tee_kaik(X,Y,X1,Y1).



kas_saab_votta(X, Y, X1, Y1, X2, Y2):- 
    (myColor(MyColor),
    ruut(X, Y, MyColor),
    tavaline_votmine(X, Y, X1, Y1, X2, Y2));
    (myColor(MyColor),
    Color is MyColor * 10,
    ruut(X, Y, Color),
    tammi_votmine(X, Y, X1, Y1, X2, Y2)).

kas_saab_kaia(X, Y, X1, Y1):-
    (myColor(MyColor),
     MyColor is 1,
     kai_ules(X, Y, X1, Y1));
    (myColor(MyColor),
     MyColor is 2,
     kai_alla(X, Y, X1, Y1));
    (ruut(X, Y, MyColor),
    (MyColor is 10; MyColor is 20),
    kas_tamm_saab_kaia(X, Y, X1, Y1)).


%-------------------------------- VÕTMINE
tavaline_votmine(X, Y, X1, Y1, X2, Y2):-
    (X1 is X - 1,
     Y1 is Y + 1,
     kas_saab_votta2(X, Y, X1, Y1, X2, Y2));
    (X1 is X - 1,
     Y1 is Y - 1,
     kas_saab_votta2(X, Y, X1, Y1, X2, Y2));
    (X1 is X + 1,
     Y1 is Y + 1,
     kas_saab_votta2(X, Y, X1, Y1, X2, Y2));
    (X1 is X + 1,
     Y1 is Y - 1,
     kas_saab_votta2(X, Y, X1, Y1, X2, Y2)).


tammi_votmine(X, Y, X1, Y1, X2, Y2):-
    vaata_ap(X, Y, X1, Y1), kas_saab_votta2(X, Y, X1, Y1, X2, Y2);
    vaata_av(X, Y, X1, Y1), kas_saab_votta2(X, Y, X1, Y1, X2, Y2);
    vaata_up(X, Y, X1, Y1), kas_saab_votta2(X, Y, X1, Y1, X2, Y2);
    vaata_uv(X, Y, X1, Y1), kas_saab_votta2(X, Y, X1, Y1, X2, Y2).



:-dynamic current/2.

vaata_ap(X, Y, X1, Y1):-
    myColor(MyColor),
    assert(current(X, Y)),
    repeat,
    current(Xt, Yt),
    retract(current(_, _)),
    Xt2 is Xt - 1, Yt2 is Yt + 1,
    assert(current(Xt2, Yt2)),
    (ruut(Xt2, Yt2 , EnemyColor),
     EnemyColor =\= MyColor, EnemyColor =\= 0;
     Xt2 = 0; Yt2 = 9),
    X1 is Xt2, Y1 is Yt2, retractall(current(_, _)), !.

vaata_av(X, Y, X1, Y1):-
    myColor(MyColor),
    assert(current(X, Y)),
    repeat,
    current(Xt, Yt),
    retract(current(_, _)),
    Xt2 is Xt - 1, Yt2 is Yt - 1,
    assert(current(Xt2, Yt2)),
    (ruut(Xt2, Yt2 , EnemyColor),
     EnemyColor =\= MyColor, EnemyColor =\= 0;
     Xt2 = 0; Yt2 = 0),
    X1 is Xt2, Y1 is Yt2, retractall(current(_, _)), !.

vaata_up(X, Y, X1, Y1):-
    myColor(MyColor),
    assert(current(X, Y)),
    repeat,
    current(Xt, Yt),
    retract(current(_, _)),
    Xt2 is Xt + 1, Yt2 is Yt + 1,
    assert(current(Xt2, Yt2)),
    (ruut(Xt2, Yt2 , EnemyColor),
     EnemyColor =\= MyColor, EnemyColor =\= 0;
     Xt2 = 9; Yt2 = 9),
    X1 is Xt2, Y1 is Yt2, retractall(current(_, _)), !.

vaata_uv(X, Y, X1, Y1):-
    myColor(MyColor),
    assert(current(X, Y)),
    repeat,
    current(Xt, Yt),
    retract(current(_, _)),
    Xt2 is Xt + 1, Yt2 is Yt - 1,
    assert(current(Xt2, Yt2)),
    (ruut(Xt2, Yt2 , EnemyColor),
     EnemyColor =\= MyColor, EnemyColor =\= 0;
     Xt2 = 9; Yt2 = 0),
    X1 is Xt2, Y1 is Yt2, retractall(current(_, _)), !.



kas_saab_votta2(X, Y, X1, Y1, X2, Y2):-
    ruut(X, Y, MyColor),
    ruut(X1, Y1, Color), 
    Color =\= MyColor, Color =\= 0,
    ((X1 > X, Xt is X1 - 1, X2 is X1 + 1);
        (X1 < X, Xt is X1 + 1, X2 is X1 - 1)),
    ((Y1 > Y, Yt is Y1 - 1, Y2 is Y1 + 1);
        (Y1 < Y, Yt is Y1 + 1, Y2 is Y1 - 1)),
    ruut(X2, Y2, 0),
    kas_tee_on_vaba(X, Y, Xt, Yt).


vota(X, Y, X1, Y1, X2, Y2):-
    retract(ruut(X, Y, MyColor)),
    assert(ruut(X,Y, 0)),
    retract(ruut(X1, Y1, _)),
    assert(ruut(X1, Y1, 0)),
    retract(ruut(X2, Y2, 0)),
    ((((X2 is 8, MyColor is 1);(X2 is 1, MyColor is 2)),
    Tamm is MyColor * 10,
    assert(ruut(X2, Y2, Tamm)));
    assert(ruut(X2, Y2, MyColor))).

%-------------------------------- KÄIMINE
kai_ules(X, Y, X1, Y1):-
    (Xt is X + 1, Yt is Y + 1,
     ruut(Xt, Yt, 0), X1 is Xt, Y1 is Yt);
    (Xt is X + 1, Yt is Y - 1,
     ruut(Xt, Yt, 0), X1 is Xt, Y1 is Yt).

kai_alla(X, Y, X1, Y1):-
    (Xt is X - 1, Yt is Y + 1,
     ruut(Xt, Yt, 0), X1 is Xt, Y1 is Yt);
    (Xt is X - 1, Yt is Y - 1,
     ruut(Xt, Yt, 0), X1 is Xt, Y1 is Yt).


kas_tamm_saab_kaia(X,Y,X1,Y1):-
    kai_ules(X, Y, X1, Y1);
    kai_alla(X, Y, X1, Y1).


kaimine(X,Y,X1,Y1):-
    ruut(X, Y, Color), myColor(MyColor),
    (Color is MyColor ; Color is MyColor * 10),
    kaik(X, Y, X1, Y1), !.


kaik(X, Y, X1, Y1):-
    kas_tee_on_vaba(X, Y, X1, Y1),
    tee_kaik(X, Y, X1, Y1).


kas_tee_on_vaba(X, Y, X, Y).
kas_tee_on_vaba(X, Y, X1, Y1):-
    ((X1 > X, Xt is X + 1);
        (X1 < X, Xt is X - 1)),
    ((Y1 > Y, Yt is Y + 1);
        (Y1 < Y, Yt is Y - 1)),
    ruut(Xt, Yt, 0),
    kas_tee_on_vaba(Xt, Yt, X1, Y1).


tee_kaik(X, Y, X1, Y1):-
    retract(ruut(X1, Y1, 0)),
    assert(ruut(X, Y, 0)),
    retract(ruut(X, Y, MyColor)),
    ((((X1 is 8, MyColor is 1);(X1 is 1, MyColor is 2)),
    Tamm is MyColor * 10,
    assert(ruut(X1, Y1, Tamm)));
    assert(ruut(X1, Y1, MyColor))).


%---------MÄNGU ALGSEIS-------------
set_up_board:-
    % Valged
    assert(ruut(1,1,0)),
    assert(ruut(1,3,20)),
    assert(ruut(1,5,0)),
    assert(ruut(1,7,1)),
    assert(ruut(2,2,1)),
    assert(ruut(2,4,0)),
    assert(ruut(2,6,1)),
    assert(ruut(2,8,1)),
    assert(ruut(3,1,2)),
    assert(ruut(3,3,1)),
    assert(ruut(3,5,0)),
    assert(ruut(3,7,1)),
    % Tühjad ruudud
    assert(ruut(4,2,0)),
    assert(ruut(4,4,0)),
    assert(ruut(4,6,0)),
    assert(ruut(4,8,1)),
    assert(ruut(5,1,0)),
    assert(ruut(5,3,0)),
    assert(ruut(5,5,0)),
    assert(ruut(5,7,0)),
    % Mustad
    assert(ruut(6,2,0)),
    assert(ruut(6,4,0)),
    assert(ruut(6,6,2)),
    assert(ruut(6,8,2)),
    assert(ruut(7,1,0)),
    assert(ruut(7,3,0)),
    assert(ruut(7,5,2)),
    assert(ruut(7,7,2)),
    assert(ruut(8,2,0)),
    assert(ruut(8,4,10)),
    assert(ruut(8,6,0)),
    assert(ruut(8,8,2)).


%=================== Print checkers board v2 - Start ==================
status_sq(ROW,COL):-
    (   ruut(ROW,COL,COLOR),
        write(' '), write(COLOR), write(' ')
    );(
        write(' . ')
    ).
status_row(ROW):-
    write('row # '), write('   '),
    status_sq(ROW,1),
    status_sq(ROW,2),
    status_sq(ROW,3),
    status_sq(ROW,4),
    status_sq(ROW,5),
    status_sq(ROW,6),
    status_sq(ROW,7),
    status_sq(ROW,8),
    nl.
% print the entire checkers board..
status:-
    nl,
    status_row(8),
    status_row(7),
    status_row(6),
    status_row(5),
    status_row(4),
    status_row(3),
    status_row(2),
    status_row(1), !.

%=================== Print checkers board v2 - End ====================

