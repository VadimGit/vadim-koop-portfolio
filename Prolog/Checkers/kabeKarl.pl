:- module(iapb142679, [iapb142679/3]). 

iapb142679(Color,X,Y) :-
    X == Y, Y == 0, main(Color);
    leia_suund(Color,Suund),
	(Color > 2,votmine_tammiga(X,Y,Suund,X1,Y1);votmine(X,Y,Suund,X1,Y1)),!.

iapb142679(_,_,_). 

:- dynamic ruut1/3.
	
main(MyColor):- %main tamm võtab
    Color is MyColor * 10, ruut(X,Y, Color),
    leia_suund(MyColor,Suund),
    kaigu_variant_vota(X,Y,Suund,X1,Y1),!.

main(MyColor):- %main nupp võtab
    ruut(X,Y, MyColor),
    leia_suund(MyColor,Suund), 
    kaigu_variant_vota(X,Y,Suund,X1,Y1),!.

main(MyColor):- %main tamm käib
    Color is MyColor * 10, ruut(X,Y,Color),
    leia_suund(MyColor, Suund),
    kaigu_variant(X,Y,Suund, X1,Y1),!.

main(MyColor):- %main nupp käib
    ruut(X,Y, MyColor),
    leia_suund(MyColor, Suund),
    kaigu_variant(X,Y,Suund,X1,Y1),!.
main(_).
 
leia_suund(1,1):- !.  
leia_suund(10,1):- !.  
leia_suund(2,-1):- !.  
leia_suund(20,-1).  
  
%--------------------------------
	
kaigu_variant_vota(X,Y,Suund,X1,Y1):-
	ruut(X,Y,Color),(Color > 2,votmine_tammiga(X,Y,Suund,X1,Y1);votmine(X,Y,Suund,X1,Y1)),
    nl, write(['votmine ']),!.
kaigu_variant(X,Y,Suund,X1,Y1):-
    ruut(X,Y,Color), (Color > 2, (kaimine(X,Y,1,X1,Y1);kaimine(X,Y,-1,X1,Y1)));kaimine(X,Y,Suund,X1,Y1),
    nl, write(['tegin kaigu']),!.
%--------------------------------
votmine(X,Y,Suund,X1,Y1):-
    kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2),
    vota(X,Y,Suund,X1,Y1,X2,Y2). 
 
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2):-  % Votmine edasi paremale
    ruut(X,Y,MyColor),
    X1 is X + Suund,
    Y1 is Y + 1,
    ruut(X1,Y1,Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2):-  % Votmine edasi vasakule
	ruut(X,Y,MyColor),
    X1 is X + Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0).
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2):-  % Votmine tagasi paremale
	ruut(X,Y,MyColor),
    X1 is X + Suund * -1,
    Y1 is Y + 1,
    ruut(X1,Y1, Color), 
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0).
kas_saab_votta(X,Y,Suund,X1,Y1,X2,Y2):-  % Votmine tagasi vasakule
	ruut(X,Y,MyColor),
    X1 is X + Suund * -1,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund * -1,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0). 
    
vota(X,Y,Suund,X1,Y1,X2,Y2) :-
	retract(ruut(X,Y,MyColor)),
	retract(ruut(X1,Y1,_)),
	retract(ruut(X2,Y2,_)), 
	assert(ruut(X,Y,0)),
	assert(ruut(X1,Y1,0)),
	assert(ruut(X2,Y2,MyColor)),
    muuda_tammiks_kui_saab(X2,Y2,MyColor),!.
%--------------------------------
kaimine(X,Y,Suund,X1,Y1):-
    kas_naaber_vaba(X,Y,Suund,X1,Y1),
    tee_kaik(X,Y,X1,Y1),
    ruut(X1,Y1,Color),
    muuda_tammiks_kui_saab(X1,Y1,Color).

kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, 0).
kas_naaber_vaba(X,Y,Suund,X1,Y1):-
    X1 is X + Suund,
    Y1 is Y -1,
    ruut(X1,Y1, 0).

kas_naaber_vaba(X,Y,X1,Y1):-
    ruut(X,Y, Status),
    assert(ruut1(X1,Y1, Status)),!.
    
tee_kaik(X,Y,X1,Y1) :-
	retract(ruut(X,Y,MyColor)),
	retract(ruut(X1,Y1,_)),
	assert(ruut(X,Y,0)),
	assert(ruut(X1,Y1,MyColor)).
	
muuda_tammiks_kui_saab(Rida,Veerg,Varv):-
	(Varv == 1, Rida == 8;
	Varv == 2, Rida == 1),
	retract(ruut(Rida,Veerg,Varv)),C is Varv*10, assert(ruut(Rida,Veerg,C));true. 
	
	
kas_saab_votta_tammiga(X,Y,Suund,X1,Y1,X2,Y2):-  % edasi paremale
	(Suund == -1, MyColor = 2; Suund == 1, MyColor = 1),
    X1 is X + Suund,
    Y1 is Y + 1,
    ruut(X1,Y1,Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0),!.
     
kas_saab_votta_tammiga(X,Y,S,X1,Y1,X2,Y2):-  %tagasi paremale
	(S == -1, MyColor = 2, Suund = 1; S == 1, MyColor = 1, Suund = -1),
    X1 is X + Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 + 1,
    ruut(X2,Y2, 0),!. 
    
kas_saab_votta_tammiga(X,Y,Suund,X1,Y1,X2,Y2):-  %ette vasakule
	(Suund == -1, MyColor = 2; Suund == 1, MyColor = 1),
    X1 is X + Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0,
    X2 is X1 + Suund,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0),!.
    
    
kas_saab_votta_tammiga(X,Y,S,X1,Y1,X2,Y2):-  %taha vasakule
	(S == -1, MyColor = 2, A = 1; S == 1, MyColor = 1, A = -1),
    X1 is X + A,
    Y1 is Y - 1,
    ruut(X1,Y1, Color),
    Color =\= MyColor, Color =\= 0, 
    X2 is X1 + A,
    Y2 is Y1 - 1,
    ruut(X2,Y2, 0),!.
    
    
votmine_tammiga(X,Y,Suund,X1,Y1):-  
	((parem_diagonaal(X,Y,Suund,X11,Y11), kas_saab_votta_tammiga(X11,Y11,Suund,X1,Y1,X2,Y2),!;
	parem_diagonaal(X,Y,-1*Suund,X11,Y11), kas_saab_votta_tammiga(X11,Y11,Suund,X1,Y1,X2,Y2),!;
	vasak_diagonaal(X,Y,Suund,X11,Y11), kas_saab_votta_tammiga(X11,Y11,Suund,X1,Y1,X2,Y2),!;
	vasak_diagonaal(X,Y,-1*Suund,X11,Y11), kas_saab_votta_tammiga(X11,Y11,Suund,X1,Y1,X2,Y2),!),
    vota(X,Y,Suund,X1,Y1,X2,Y2)). 

parem_diagonaal(X,Y,Suund,X,Y):-
	X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1,C), not(C == 0),!.
	
parem_diagonaal(X,Y,Suund,X2,Y2):-
    X1 is X +Suund,
    Y1 is Y + 1,
    ruut(X1,Y1, C), C == 0, 
    parem_diagonaal(X1,Y1,Suund,X2,Y2),!.
    
vasak_diagonaal(X,Y,Suund,X,Y):-
	X1 is X +Suund,
    Y1 is Y - 1,
    ruut(X1,Y1,C), not(C == 0),!.
	
vasak_diagonaal(X,Y,Suund,X2,Y2):-
    X1 is X +Suund,
    Y1 is Y - 1,
    ruut(X1,Y1, C), C == 0,
    vasak_diagonaal(X1,Y1,Suund,X2,Y2),!.

%=================== Print checkers board - Start ==================
print_board :-
	print_squares(8). 

print_squares(Row) :-
	between(1, 8, Row),
	write('|'), print_row_squares(Row, 1), write('|'), nl,
	NewRow is Row - 1,
	print_squares(NewRow), !.
print_squares(_) :- !.


print_row_squares(Row, Col) :-
	between(1, 8, Col),
	ruut(Col, Row, Status), write(' '), write(Status), write(' '),
	NewCol is Col + 1,
	print_row_squares(Row, NewCol), !.
print_row_squares(_, _) :- !.

%=================== Print checkers board - End ====================

%=================== Print checkers board v2 - Start ==================
status_sq(ROW,COL):-
	(	ruut(ROW,COL,COLOR),
		write(COLOR)
	);(
		write(' ')
	).
status_row(ROW):-
	write('row # '),write(ROW), write('   '),
	status_sq(ROW,1),
	status_sq(ROW,2),
	status_sq(ROW,3),
	status_sq(ROW,4),
	status_sq(ROW,5), 
	status_sq(ROW,6),
	status_sq(ROW,7),
	status_sq(ROW,8), 
	nl.
% print the entire checkers board..
status:-
	nl,
	status_row(8),
	status_row(7),
	status_row(6),
	status_row(5),
	status_row(4),
	status_row(3),
	status_row(2),
	status_row(1).

%=================== Print checkers board v2 - End ====================