
is_a(karu, omnivoor).
is_a(tulp, oistaim).
is_a(oistaim, taim).
is_a(rebane, karnivoor).
is_a(lehm, herbivoor).
is_a(maasi, lehm).
is_a(lehm, loom).
is_a(kapsas, mitteoistaim).
is_a(mitteoistaim, taim).
%is_a(kana, herbivoor).
is_a(kana, loom).
 
soob(herbivoor, taim).
soob(karnivoor, loom).
soob(omnivoor, taim).
soob(omnivoor, loom).
 
% ----------------- Klasside omadused ---------
loom(eluvorm).
taim(eluvorm).
 
eluvorm(hingab).
eluvorm(toitub).
eluvorm(paljuneb).
eluvorm(kasvab).
 
loom(herbivoor).
loom(omnivoor).
loom(karnivoor).
 
taim(oistaim).
taim(mitteoistaim).
 
       
loomne_toit(loom).
taimne_toit(taim).
 

soob(Kes, Keda, Millal):-
        is_a(Kes, Ylemklass),
        not(is_a(Keda, Ylemklass)),
        ulemklass(Kes, Ylemklass1),
        ulemklass(Keda, Ylemklass2),
        soob(Ylemklass1, Ylemklass2),
        (loomne_toit(Ylemklass2), (Millal >=22; Millal=< 6), Millal >=0, Millal < 24;
        taimne_toit(Ylemklass2), Millal > 6, Millal < 22).
 

ulemklass(Kes, Kelle):-
        is_a(Kes, Kelle).
ulemklass(Kes, Kelle):-
        is_a(Kes, Vahepealne),
        ulemklass(Vahepealne, Kelle).
		