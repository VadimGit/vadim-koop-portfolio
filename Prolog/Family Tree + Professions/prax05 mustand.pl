male(henri).	%Ancestors
female(tiina).
male(martin).
female(rita).
male(kork).
female(hele).
male(jesica).
female(sam).

male(aso). %GrandfatherOfKristinaAndKrista
female(maria). %GrandmotherOfKristinaAndKrista
male(vadim). %FatherOfVladAndKristi
male(vladimir). %BrotherOfVadim

male(vlad). %son
female(kristi). %daughter

female(marianna). %GrandmotherOfKristinaAndKrista
male(karel). %GrandfatherOfKristinaAndKrista
female(kristina). %MotherOfVladAndKristi
female(krista). %SisterOfKristina

%-----------------families--------------------%

married(tiina,henri). %Ancestors
married(rita,martin).
married(hele,kork).
married(jesica,sam).

mother(maria,tiina).
mother(aso,rita).
mother(marianna,hele).
mother(karel,jesica).

married(maria,aso). %Family1
mother(vadim,maria).
mother(vladimir,maria).

married(marianna,karel). %Family2
mother(kristina,marianna).
mother(krista,marianna).

married(kristina,vadim). %Generetion
mother(vlad,kristina).
mother(kristi,kristina).

%-----------------functions--------------------%

father(Child, Father):-
	married(Mother, Father),
	mother(Child, Mother).
	
brother(Child, Brother):-
	male(Brother),
	mother(Child, Mother),
	mother(Brother,Mother),
	Child \= Brother.
	
sister(Child, Sister):-
	female(Sister),
	mother(Child, Mother),
	mother(Sister, Mother),
	Child \= Sister.
	
aunt(Child, Aunt):-
	mother(Child, Mother), sister(Mother, Aunt);
	father(Child, Father), sister(Father, Aunt).
	
uncle(Child, Uncle):-
	mother(Child, Mother), brother(Mother, Uncle);
	father(Child, Father), brother(Father, Uncle).
	
grandmother(Child, Grandmother):-
	mother(Child, Mother), mother(Mother, Grandmother);
	father(Child, Father), mother(Father, Grandmother).

grandfather(Child, Grandfather):-
	mother(Child, Mother), father(Mother, Grandfather);
	father(Child, Father), father(Father, Grandfather).
%fatherOffatherqualGrandfather	

%-----------------FunctionsAncestor--------------------%

female_ancestor(Child, Grandgrandmother):-
mother(Child, Grandgrandmother). 
female_ancestor(Child, Grandgrandmother):-
mother(Child, Mother), female_ancestor(Mother, Grandgrandmother). 

male_ancestor(Child, Grandfather):-
father(Child, Grandfather). 
male_ancestor(Child, Grandfather):-
father(Child, Father), male_ancestor(Father, Grandfather).

ancestor(X, Y, 1):- child(X, Y).
	ancestor(X, Y, N):-
		N1 is N - 1,
		child(X, Z),
		ancestor(Z, Y, N1).

ancestor2(Child, Parent, X).
	
%-----------------AmetiBaas--------------------%

%kaks1taseme
is_a('Ametid', 'Juhid').
is_a('Ametid', 'Tippspetsialistid').

%neli2taseme
is_a('Juhid', 'Seadusandjad, kõrgemad ametnikud ja juhid').
is_a('Juhid', 'Juhid äri- ja haldusalal').

is_a('Tippspetsialistid', 'Loodus- ja tehnikateaduste tippspetsialistid').
is_a('Tippspetsialistid', 'Tervishoiu tippspetsialistid').

%kumme4taseme
is_a('Juhid', 'Seadusandjad').
is_a('Juhid', 'Kõrgemad valitsusametnikud').
is_a('Juhid', 'Aleviku- ja külavanemad').
is_a('Juhid', 'Kõrgemad ametnikud ühiskondlikes huviorganisatsioonides').
is_a('Juhid', 'Juhid finantsalal').

is_a('Tippspetsialistid', 'Füüsikud ja astronoomid').
is_a('Tippspetsialistid', 'Meteoroloogid').
is_a('Tippspetsialistid', 'Keemikud').
is_a('Tippspetsialistid', 'Geoloogid ja geofüüsikud').
is_a('Tippspetsialistid', 'Erialase kõrgharidusega parameedikud').

%-----------------InimisteAmetid--------------------%

is_a(tiina, 'Juhid').
is_a(henri, 'Tippspetsialistid').
is_a(rita, 'Seadusandjad, kõrgemad ametnikud ja juhid').
is_a(martin, 'Juhid äri- ja haldusalal').
is_a(hele, 'Loodus- ja tehnikateaduste tippspetsialistid').
is_a(kork, 'Tervishoiu tippspetsialistid').
is_a(jesica, 'Seadusandjad').
is_a(sam, 'Kõrgemad valitsusametnikud').
is_a(maria, 'Aleviku- ja külavanemad').
is_a(aso, 'Kõrgemad ametnikud ühiskondlikes huviorganisatsioonides').
is_a(marianna, 'Juhid finantsalal').
is_a(karel, 'Füüsikud ja astronoomid').
is_a(vladimir, 'Meteoroloogid').
is_a(vadim, 'Keemikud').
is_a(kristina, 'Geoloogid ja geofüüsikud').
is_a(krista, 'Erialase kõrgharidusega parameedikud').

%-----------------FunctionAmet--------------------%

occupation().

who_is(O, Who).

alamklass(Kes, Kelle):-
	is_a(Kes,Kelle),!.
alamklass(Kes, Kelle):-
	is_a(Kes,Vahepealne),
	alamklass(Vahepealne,Kelle).

alamhulk(X,Y):-  is_a(X,Y).
alamhulk(X,Y):-  is_a(W,Y), alamhulk(X,W).

%----------------FunctionsMBY--------------------%

soob(Kes, Keda, Millal):-
        is_a(Kes, Ylemklass),
        not(is_a(Keda, Ylemklass)),
        ulemklass(Kes, Ylemklass1),
        ulemklass(Keda, Ylemklass2),
        soob(Ylemklass1, Ylemklass2),
        (loomne_toit(Ylemklass2), (Millal >=22; Millal=< 6), Millal >=0, Millal < 24;
        taimne_toit(Ylemklass2), Millal > 6, Millal < 22).
 

ulemklass(Kes, Kelle):-
        is_a(Kes, Kelle).
ulemklass(Kes, Kelle):-
        is_a(Kes, Vahepealne),
        ulemklass(Vahepealne, Kelle).
