laevaga(tallinn, helsinki, 120, time(12, 45, 0.0), time(14, 45, 0.0)).
laevaga(tallinn, stockholm, 480, time(10, 45, 0.0), time(15, 45, 0.0)).
laevaga(helsinki, tallinn, 120, time(16, 45, 0.0), time(18, 45, 0.0)).
laevaga(stockholm, tallinn, 480, time(18, 45, 0.0), time(23, 45, 0.0)).
bussiga(tallinn, riia, 300, time(9, 10, 0.0), time(12, 45, 0.0)).
bussiga(riia, tallinn, 300, time(15, 40, 0.0), time(19, 40, 0.0)).
rongiga(riia, berlin, 680, time(12, 30, 0.0), time(23, 30, 0.0)).
rongiga(berlin, riia, 680, time(8, 20, 0.0), time(19, 20, 0.0)).
lennukiga(tallinn, helsinki, 300, time(12, 45, 0.0), time(13, 15, 0.0)).
lennukiga(helsinki, tallinn, 300, time(18, 0, 0.0), time(18, 40, 0.0)).
lennukiga(helsinki, paris, 180, time(12, 45, 0.0), time(15, 30, 0.0)).
lennukiga(paris, helsinki, 180, time(8, 45, 0.0), time(11, 45, 0.0)).
lennukiga(berlin, paris, 120, time(12, 0, 0.0), time(13, 30, 0.0)).
lennukiga(paris, berlin, 120, time(8, 45, 0.0), time(10, 0, 0.0)).

earlier(T1, T2) :- compare((<), T1, T2).
:- dynamic labitud/1.

reisi(A, B, mine(A,B,Transport), Hind) :-
	leia_tee(A,B,Hind,_,_,Transport),!.
	
reisi(A, B, mine(A, X, Transport, Path), Sum) :-
	assert(labitud(A)),
	leia_tee(A, X, Hind, _, _, Transport),
	not(labitud(X)),assert(labitud(X)),
	reisi(X, B, Path, HNext),
	Sum is HNext + Hind,
	retractall(labitud(X)).
	
reisi(A,_,mine(A,_,_,_),_):-
	retractall(labitud(A)),
	fail.
	
odavaim_reis(A,B,Tee,Hind) :-
	reisi(A,B,Tee,Hind),
	not((reisi(A,B,_,HNext),
        HNext<Hind)).

leia_tee(A,B,H,V,S,Transport) :-
	laevaga(A,B,H,V,S),Transport=laevaga;
	lennukiga(A,B,H,V,S),Transport=lennukiga;
	bussiga(A,B,H,V,S),Transport=bussiga;
	rongiga(A,B,H,V,S),Transport=rongiga.
	
	
time_diff(T1,T2,Diff):-
	time(H1,M1,S1)=T1,
	time(H2,M2,S2)=T2,
	S3 is S1-S2, M3 is M1-M2, H3 is H1-H2,
	((S3<0,S4 is S3+60,M4 is M3-1);
	(S3>=0,S4 is S3,M4 is M3)),
	((M4<0,M5 is M4+60,H4 is H3-1);
	M3>=0,M5 is M4,H4 is H3),
	H3>=0,
	Diff = time(H4, M5, S4).
	
time_sum(T1,T2,Sum):-
	time(H1,M1,S1)=T1,
	time(H2,M2,S2)=T2,
	S3 is S1+S2,M3 is M1+M2,H3 is H1+H2,
	((S3 >= 60,S4 is S3-60,M4 is M3+1);
	(S3<60,S4 is S3,M4 is M3)),
	((M4>=60,M5 is M4-60,H4 is H3+1);
	(M4<60,M5 is M4,H4 is H3)),
	Sum=time(H4,M5,S4).
	
time_sum(T1,T2,T3,Sum):-
	time_sum(T1,T2,Sum1),
	time_sum(Sum1,T3,Sum).
	
time_between(V,S,Time):-
	((time_diff(S,V,Time),!);
	(time_sum(S,time(24,0,0.0),NextDay),
	time_diff(NextDay,V,Time))).
	
time_for_wait(S,V,Time):-
	time_between(S,V,T1),
	time(H1, M1, S1)=T1,
	((H1 > 0, H2 is H1);(H1 is 0, H2 is 24)),
	Time = time(H2, M1, S1).
	
tee_ajaga(A,B,Transport,H,Time,V,S):-
	leia_tee(A,B,H,V,S,Transport),
	time_between(V,S,Time).
	
reisi_ajaga(A,B,mine(A,B,Transport),H,Time,V,S):-
	tee_ajaga(A,B,Transport,H,Time,V,S),!.
	
reisi_ajaga(A,B,mine(A,B,Transport,Tee),H,Time,V,S):-
	assert(labitud(A)),
	tee_ajaga(A,Peatus,Transport,H1,T1,V,S1),
	not(labitud(Peatus)),assert(labitud(Peatus)),
	reisi_ajaga(Peatus,B,Tee,H2,T2,V2,S),
	H is H1 + H2,
	time_for_wait(S1,V2,T3),
	time_sum(T1,T2,T3,Time),
	retractall(labitud(Peatus)).
	
reisi_ajaga(A,_,mine(A,_,_,_),_,_,_,_):-
	retractall(labitud(A)),
	fail.
	
lyhim_reis(A,B,Tee,H) :-
	reisi_ajaga(A,B,Tee,H,T1,_,_),
	not((reisi_ajaga(A,B,_,_,T2,_,_),
        time_diff(T1,T2,Diff),
        time(H1,M1,S1)=Diff,(S1+M1+H1)>0)).
