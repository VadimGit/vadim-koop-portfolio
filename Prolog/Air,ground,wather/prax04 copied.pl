%========================================1======================================================
laevaga(tallinn, helsinki, 120, time(12, 45, 0.0), time(14, 45, 0.0)).
laevaga(tallinn, stockholm, 480, time(13, 10, 0.0), time(16, 30, 0.0) ).
bussiga(tallinn, riia, 300,time(11, 55, 0.0), time(12, 40, 0.0) ).
rongiga(riia, berlin, 680,time(3, 25, 0.0), time(5, 30, 0.0) ).
lennukiga(tallinn, helsinki, time(11, 55, 0.0), time(12, 40, 0.0) ).
lennukiga(helsinki, paris, 180,(3, 25, 0.0), time(5, 30, 0.0)).
lennukiga(paris, berlin, 120,(3, 25, 0.0), time(5, 30, 0.0)).

   :-  dynamic  labitud/1. 

reisima2(Kust, Kuhu, Transport, Hind, AlgAeg, LAeg) :- laevaga(Kust,Kuhu,Hind,AlgAeg,LAeg), Transport = laevaga.
reisima2(Kust, Kuhu, Transport, Hind, AlgAeg, LAeg) :- rongiga(Kust,Kuhu,Hind,AlgAeg,LAeg), Transport = rongiga.
reisima2(Kust, Kuhu, Transport, Hind, AlgAeg, LAeg) :- lennukiga(Kust,Kuhu,Hind,AlgAeg,LAeg), Transport = lennukiga.
reisima2(Kust, Kuhu, Transport, Hind, AlgAeg, LAeg) :- bussiga(Kust,Kuhu,Hind,AlgAeg,LAeg), Transport = bussiga.

reisima2(Kust, Kuhu, Transport, Hind, Aeg, AlgAeg, LAeg) :- reisima2(Kust, Kuhu, Transport, Hind, AlgAeg, LAeg),
		 aeg_hetkede_vahel(AlgAeg, LAeg, Aeg).
%================================================2=================================================================
reisi(Kust, Kuhu, mine(Kust, Kuhu, Transport), Hind) :- reisima2(Kust, Kuhu, Transport, Hind, _, _), !.

reisi(Kust, Kuhu, mine(Kust, Vahepeatus, Transport, Path), Hind, _) :-
	reisima2(Kust, Vahepeatus, Transport, Xv, _, _),
	not(labitud(Vahepeatus)), assert(labitud(Vahepeatus)),
	reisi(Vahepeatus, Kuhu, Path, Vz),
	retract(labitud(Vahepeatus)),
	Hind is Xv + Vz.

%================================================3=============================================================
odavaim_reis(From, To, Tee, Hind) :-
        assert(min_hind(9999)),
        leia_odavaim(From, To, Tee, Hind);
        parim_reis(From,To,Tee,Hind).
       
lyhim_reis(From, To, Tee, Hind) :-
        assert(min_aeg(time(999, 99, 0.0))),
        leia_lyhim(From, To, Tee, Hind, Aeg);
        parim_reis(From,To,Tee,Hind).
       
leia_odavaim(From, To, Tee, Hind) :-
        reisi(From, To, Tee, Hind),
        retractall(parim_reis(_,_,_,_)),
        assert(parim_reis(From, To, Tee, Hind)),
        min_hind(H_parim),
        Hind < H_parim,
        retract(min_hind(_)),
        assert(min_hind(Hind)),
        fail.
       
leia_lyhim(From, To, Tee, Hind, Aeg) :-
        reisi(From, To, Tee, Hind, Aeg),
        retractall(parim_reis(_,_,_,_)),
        assert(parim_reis(From, To, Tee, Hind)),
        min_aeg(A_parim),
        Aeg < A_parim,
        retract(min_aeg(_)),  
        assert(min_aeg(Aeg)),
        fail.
       
aegade_vahe(Aeg1, Aeg2, Vahe):-
        time(H1,M1,S1) = Aeg1,
        time(H2,M2,S2) = Aeg2,
        Vahe is Aeg2 - Aeg1.
       
 tyhjenda() :-
         retractall(labitud(_)),
         retractall(min_hind(_)),
         retractall(min_aeg(_)),
         retractall(parim_reis(_,_,_,_)).
