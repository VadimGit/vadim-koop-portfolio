lennukiga(tallinn, riga, 420, time(22, 45, 0.0), time(23, 45, 0.0)).
lennukiga(stockholm, paris, 415, time(21, 45, 0.0), time(22, 45, 0.0)).
lennukiga(tallinn, riga, 410, time(20, 45, 0.0), time(21, 45, 0.0)).

rongiga(tallinn, copenhagen, 320, time(19, 45, 0.0), time(21, 45, 0.0)).
rongiga(stockholm, copenhagen, 315, time(18, 45, 0.0), time(20, 45, 0.0)).
rongiga(stockholm, tallinn, 310, time(17, 45, 0.0), time(19, 45, 0.0)).

bussiga(vilnus, paris, 220, time(16, 45, 0.0), time(19, 45, 0.0)).
bussiga(vilnus, tallinn, 215, time(15, 45, 0.0), time(18, 45, 0.0)).

laevaga(riga, vilnus, 120, time(14, 45, 0.0), time(19, 45, 0.0)).
laevaga(copenhagen, vilnus, 115, time(13, 45, 0.0), time(18, 45, 0.0)).
laevaga(paris, stockholm, 110, time(12, 45, 0.0), time(17, 45, 0.0)).

%--------------------------------FUNCTION1---------------------------------

odavaim_reis(From, To, Tee, Hind) :-
        assert(min_hind(9999)),
        leia_odavaim(From, To, Tee, Hind);
        parim_reis(From,To,Tee,Hind).
       
lyhim_reis(From, To, Tee, Hind) :-
        assert(min_aeg(time(999, 99, 0.0))),
        leia_lyhim(From, To, Tee, Hind, Aeg);
        parim_reis(From,To,Tee,Hind).
       
leia_odavaim(From, To, Tee, Hind) :-
        reisi(From, To, Tee, Hind),
        retractall(parim_reis(_,_,_,_)),
        assert(parim_reis(From, To, Tee, Hind)),
        min_hind(H_parim),
        Hind < H_parim,
        retract(min_hind(_)),
        assert(min_hind(Hind)),
        fail.
       
leia_lyhim(From, To, Tee, Hind, Aeg) :-
        reisi(From, To, Tee, Hind, Aeg),
        retractall(parim_reis(_,_,_,_)),
        assert(parim_reis(From, To, Tee, Hind)),
        min_aeg(A_parim),
        Aeg < A_parim,
        retract(min_aeg(_)),  
        assert(min_aeg(Aeg)),
        fail.
       
aegade_vahe(Aeg1, Aeg2, Vahe):-
        time(H1,M1,S1) = Aeg1,
        time(H2,M2,S2) = Aeg2,
        Vahe is Aeg2 - Aeg1.
       
 tyhjenda() :-
         retractall(labitud(_)),
         retractall(min_hind(_)),
         retractall(min_aeg(_)),
         retractall(parim_reis(_,_,_,_)).


