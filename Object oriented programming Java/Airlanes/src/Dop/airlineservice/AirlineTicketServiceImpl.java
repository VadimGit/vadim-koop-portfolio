package airlineservice;

import java.util.Stack;

import lufthavn.GateTicket;
import lufthavn.GoldTicket;
import lufthavn.SilverTicket;

public class AirlineTicketServiceImpl implements AirlineTicketService {
	
	 private Stack<GateTicket> gateTickets = new Stack<>(); 
	 
	 public AirlineTicketServiceImpl() {
		 gateTickets.push(new GoldTicket("GoldPax","G001",1000));
		 gateTickets.push(new SilverTicket("SilverPax","S001",500));
		 gateTickets.push(new GoldTicket("RegularPax","T001",100));
		 gateTickets.push(new GoldTicket("GoldPax","G002",1000));
		 gateTickets.push(new SilverTicket("SilverPax","S002",500));
	 }


	@Override
	public boolean hasNextTicket() {
		return gateTickets.size() > 0;
	}

	@Override
	public GateTicket getNextTicket() {
		return gateTickets.pop();
	}

}
