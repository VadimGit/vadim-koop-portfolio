package lufthavn;

public abstract class GeneralTicket implements GateTicket {

	private String passengerName;
	private String ticketCode;
	protected int ticketCost;
	
	// Konstruktor
	//inicializiruet polja klassa
	
	public GeneralTicket(String name, String code, int cost){
		passengerName= name;
		ticketCode=code;
		ticketCost=cost;
	}
	
	@Override
	public String getPassengerName() {
		// TODO Auto-generated method stub
		return passengerName;
	}
	@Override
	public String getTicketCode() {
		// TODO Auto-generated method stub
		return ticketCode;
	}

	@Override
	public int getCost() {
		// TODO Auto-generated method stub
		return getDiscount();
	}
	
	public abstract int getDiscount();

}
