/**
 * 
 */
package lufthavn;

/**
 * @author Aleksander.nesterovs
 *
 */
public class GateDataReceiverImpl implements GateDataReceiver {

	/* (non-Javadoc)
	 * @see lufthavn.GateDataReceiver#addTicket(lufthavn.GateTicket)
	 */
	@Override
	public void addTicket(GateTicket t) throws IllegalArgumentException {
		System.out.print("Registred ticket nr:" + t.getTicketCode());//ticket code
		System.out.print(":" + t.getPassengerName() );//passenger name
		System.out.println("("+ t.getCost() +")");//ticket cost

	}

}
