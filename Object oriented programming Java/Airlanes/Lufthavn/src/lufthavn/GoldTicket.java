package lufthavn;

public class GoldTicket extends GeneralTicket {

	public GoldTicket(String name, String code, int cost) {
		super(name, code, cost);
	
	}

	@Override
	public int getDiscount() {
		// TODO Auto-generated method stub
		return (int)  ( 0.5 * ticketCost );
	}

}
