package lufthavn;

public interface GateTicket {
	public String getPassengerName();
	 public String getTicketCode();
	 public int getCost(); 

}
