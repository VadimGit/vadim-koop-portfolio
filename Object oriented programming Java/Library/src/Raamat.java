import java.util.Random;
/**
 * This class of usual book.
 * @author Alexander Klevanets 090361 IAPB47
 *
 */
class Raamat{
	protected String name;
	protected String text;
	private Semaphore s = new Semaphore();
	static Random r = new Random();
	boolean check;
	/**
	 * This is constructor of the class.
	 * @param name - name of the book
	 */
	Raamat(String name){
		this.name=name;
		text="\n\t"+name+" contain:\n";
	}
	/**
	 * This method is for reading.
	 * @param name - name of current reader
	 * @return - true if is allowed to read for this reader
	 */
	public boolean read(String name){
		check=s.acquireRead(name);
		if(check==false)
			return false;
		System.out.println("--->"+name+" is reading "+this.name);
		try {
			ReaderWriter.sleep(r.nextInt(2000)+2000); 
		} catch(InterruptedException e){}
		System.out.println(text);
		System.out.println("<---"+name+ " ended reading "+this.name);
		s.releaseRead();
		return true;
	}
	/**
	 * This method is for writing.
	 * @param name - name of current writer
	 */
	public void write(String name){
		s.acquireWrite(name);
		System.out.println("--->"+name+ " is writing in "+this.name);
		try {
			ReaderWriter.sleep(r.nextInt(2000)+2000); 
		} catch(InterruptedException e){}
		text+="\tnew record by "+ name+"\n";
		System.out.println("<---"+name+" ended writing in "+this.name);
		s.releaseWrite();
	}
}