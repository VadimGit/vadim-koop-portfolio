import java.util.ArrayList;


public class Raamatukogu {
	public static void main(String[] args) {
		ArrayList<Raamat> books = new ArrayList<Raamat>();
		for(int i=1;i<4;i++){
			books.add(new Raamat("never ending story, part "+i));
		}
		for(int j=1;j<15;j++){
			if(j%3==0)
				new ReaderWriter("writer", books);
			new ReaderWriter("reader", books);
		}
	}
}
