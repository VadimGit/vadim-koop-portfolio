import java.util.HashMap;
/**
 * This is Semaphore class what controls using of the book.
 * @author Alexander Klevanets 090361 IAPB47
 *
 */
class Semaphore {
	final static int EMPTY = 0;
	final static int READING = 1;
	final static int WRITING = 2;
	protected int state=EMPTY;
	protected int readCnt=0;
	protected static HashMap<String, Integer> users = new HashMap<String, Integer>();
/**
 * This method checks if book ready for reading.
 * @param name - name of current reader
 * @return - true if had waited not more then 4 times running
 */
	public synchronized boolean acquireRead(String name) {
		if(!users.containsKey(name))
			users.put(name, 0);
		if(users.get(name)>3)
			return false;
		if (state == EMPTY) {
			state = READING;
			users.put(name, 0);
		}
		else if ((state == READING)&&(readCnt<2)) {
			users.put(name, 0);
		}
		else if ((state == WRITING)||(readCnt>1)) {
			while((state == WRITING)||(readCnt>1)) {
				int count = users.get(name)+1;
				users.put(name, count);
				System.out.println(name + " is waiting his turn, it is his "
						+users.get(name)+" turn");
				try {wait();}
				catch (InterruptedException e){}
			}
			state = READING;
		}
		readCnt++;
		return true;
	}
	/**
	 * This method checks if book ready for writing.
	 * @param name - name of writer
	 */
	public synchronized void acquireWrite(String name) {
		if (state == EMPTY) 
			state = WRITING;
		else {
			while(state != EMPTY) {
				System.out.println(name + " is waiting his turn");
				try {wait();}
				catch (InterruptedException e){}
			}
			state = WRITING;
		}
	}
	/**
	 * This method release place from reader.
	 */
	public synchronized void releaseRead() {
		readCnt--;
		if (readCnt == 0) {
			state = EMPTY;
			notify();
		}
	}
	/**
	 * This method release place from writer.
	 */
	public synchronized void releaseWrite() {
		state = EMPTY;
		notify();
	}
}
/*class Semaphore {
private int readers =0;
private boolean writing = false;

public synchronized void acquireRead(String name) {
  while (writing || readers==2) 
  	try{
  		wait();
  	}catch(InterruptedException e) {}
  ++readers;
}

public synchronized void releaseRead() {
  --readers;
  if(readers==0) notifyAll();
}

public synchronized void acquireWrite(String name) {
  while (readers>0 || writing) 
  	try{
  		wait();
  	}catch(InterruptedException e) {}
  writing = true;
}

public synchronized void releaseWrite() {
  writing = false;
  notifyAll();
}
}
*/

