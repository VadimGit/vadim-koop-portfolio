import java.util.ArrayList;

/**
 * This is class of user (reader or writer).
 * @author Alexander Klevanets 090361 IAPB47
 *
 */
class ReaderWriter extends Thread {

	String name;
	ArrayList<Raamat> books;
	boolean status=true;
	/**
	 * This is constructor of the class.
	 * @param name - name of user, also shows what he will do
	 * @param book1 - first book from library
	 * @param book2 - second book from library
	 */
	ReaderWriter(String name, ArrayList<Raamat> books) {
		super(name);
		this.name=name;
		this.books=books;
		start();

	}
	/**
	 * This method is starts Thread.
	 */
	public void run() {
		name+=getId();
		while(status){
			int a = Raamat.r.nextInt(books.size());
			if (name.startsWith("reader")) {		
				System.out.println(name+" wants to read "+books.get(a).name);
				status=books.get(a).read(name);
				try {
					sleep(Raamat.r.nextInt(2000)+2000); 
					} catch(InterruptedException e){}
				}
			else if (name.startsWith("writer")) {
					System.out.println(name+" wants to write something in "+books.get(a).name);
					books.get(a).write(name);
				try {
					sleep(Raamat.r.nextInt(1000)+1000); 
				} catch(InterruptedException e){}
			}
		}
		System.out.println(name+" was killed by others angry readers!");
	}
}
