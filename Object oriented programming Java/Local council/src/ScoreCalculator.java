/**
 * Created by VADIM on 17-Oct-17.
 */
public class ScoreCalculator {

    public static double getIndividualScore(int votes, int totalVotes) {
        if(totalVotes == 0) return 0;
        if(votes < 0 || totalVotes < 0) {
            throw new IllegalArgumentException("Votes cannot be negative numbers");
        }
        if(votes > totalVotes) {
            throw new IllegalArgumentException("Individual votes cannot be larger than total votes");
        }
        return (double)votes / (double)totalVotes * 100.0f;
    }

    public static double getCompensationScore(int votes, int totalVotes) {
        if(totalVotes == 0) return 0;
        if(votes < 0 || totalVotes < 0) {
            throw new IllegalArgumentException("Votes cannot be negative numbers");
        }
        if(votes > totalVotes) {
            throw new IllegalArgumentException("Individual votes cannot be larger than total votes");
        }
        return (double)votes / (double)totalVotes * 100.0f / 1.5f;
    }
}
