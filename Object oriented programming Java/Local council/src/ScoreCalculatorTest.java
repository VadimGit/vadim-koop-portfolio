import org.junit.Assert;
import org.junit.Test;
/**
 * Created by VADIM on 17-Oct-17.
 */
public class ScoreCalculatorTest {

    @Test
    public void testGetIndividualScore() throws Exception {
        double score = ScoreCalculator.getIndividualScore(10, 100);
        Assert.assertEquals(10.0f, score, 0.001f);
    }

    @Test
    public void testGetCompensationScore() throws Exception {
        double score = ScoreCalculator.getCompensationScore(15, 100);
        Assert.assertEquals(10.0f, score, 0.001f);
    }
}