import java.util.Optional;
/**
 * Created by VADIM on 17-Oct-17.
 */
public class Candidate {
    private String name;
    private Optional<String> party = Optional.empty();
    private int votes = 0;
    private double score = 0;

    /**
     * Constructors.
     */
    public Candidate(String name) {
        this.name = name;
    }

    public Candidate(String name, int votes) {
        this.name = name;
        this.votes = votes;
    }

    public Candidate(String name, int votes, String party) {
        this.name = name;
        this.votes = votes;
        this.party = Optional.of(party);
    }

    /**
     * Getters.
     */
    public String getName() {
        return name;
    }

    public int getVotes() {
        return votes;
    }

    public double getScore() {
        return score;
    }

    public Optional<String> getParty() {
        return party;
    }

    /**
     * Setters.
     */
    public void setVotes(int votes) {
        this.votes = votes;
    }

    public void setScore(double score) {
        this.score = score;
    }

    /**
     * To String.
     */
    public String toString() {
        String s = name;
        if(party.isPresent()) {
            s += " (" + party.get() + ")";
        }
        s += String.format(" has %d votes and a score of %.2f", votes, score);
        return s;
    }
}
