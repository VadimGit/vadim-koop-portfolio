/**
 * Created by VADIM on 17-Oct-17.
 */
public class Main {

    public static void main(String[] args) {
        Candidate a = new Candidate("Alice", 225); // Compensation
        Candidate b = new Candidate("Bob", 10);
        Candidate c = new Candidate("Clark", 20);
        Candidate d = new Candidate("David", 150);
        Candidate e = new Candidate("Emma", 225); // Compensation
        Candidate f = new Candidate("Frank", 100);
        Candidate g = new Candidate("George", 100);
        Candidate h = new Candidate("Hilary", 50);
        Candidate i = new Candidate("Vadim", 1500); // Individual
        Candidate j = new Candidate("Josephine", 500); // Individual
        VoteManager vm = new VoteManager(250, 5.0f);
        vm.addCandidate(a);
        vm.addCandidate(b);
        vm.addCandidate(c);
        vm.addCandidate(d);
        vm.addCandidate(e);
        vm.addCandidate(f);
        vm.addCandidate(g);
        vm.addCandidate(h);
        vm.addCandidate(i);
        vm.addCandidate(j);
        vm.processVotes();
        System.out.println("All candidates:");
        vm.printAllCandidateDetails();
        System.out.println("Winners:");
        for (String w : vm.getWinnerNames()) {
            System.out.println(w);
        }
    }
}
