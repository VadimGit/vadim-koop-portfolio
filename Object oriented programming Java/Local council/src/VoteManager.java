import java.util.ArrayList;
import java.util.List;
/**
 * Created by VADIM on 17-Oct-17.
 */
public class VoteManager {

    private int individualQuota;
    private double scoreQuota;

    private int totalVotes = 0;

    private List<Candidate> candidates = new ArrayList<>();
    private List<Candidate> winners = new ArrayList<>();

    public VoteManager(int individualQuota, double scoreQuota) {
        this.individualQuota = individualQuota;
        this.scoreQuota = scoreQuota;
    }

    public void addCandidate(Candidate c) {
        candidates.add(c);
        totalVotes += c.getVotes();
    }

    public void processVotes() {
        winners.clear();
        for(Candidate c : candidates) {
            int votes = c.getVotes();
            if(votes >= individualQuota) {
                c.setScore(ScoreCalculator.getIndividualScore(votes, totalVotes));
                winners.add(c);
            } else {
                c.setScore(ScoreCalculator.getCompensationScore(votes, totalVotes));
                if(c.getScore() >= scoreQuota) {
                    winners.add(c);
                }
            }
        }
    }

    public List<String> getWinnerNames() {
        List<String> names = new ArrayList<>();
        for(Candidate w : winners) {
            names.add(w.getName());
        }
        return names;
    }

    public void printAllCandidateDetails() {
        for(Candidate c : candidates) {
            System.out.println(c);
        }
    }

    /**
     * Getters.
     */
    public int getIndividualQuota() {
        return individualQuota;
    }

    public double getScoreQuota() {
        return scoreQuota;
    }

    public int getTotalVotes() {
        return totalVotes;
    }

    /**
     * Setters.
     */
    public void setIndividualQuota(int individualQuota) {
        this.individualQuota = individualQuota;
    }

    public void setScoreQuota(int scoreQuota) {
        this.scoreQuota = scoreQuota;
    }
    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }
}