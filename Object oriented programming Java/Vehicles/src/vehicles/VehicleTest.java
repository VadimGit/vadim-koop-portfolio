package vehicles;

import java.util.ArrayList;
import java.util.List;

public class VehicleTest {
	public static void main(String[] args) {
		
		List<Vehicle> vehicles = new ArrayList<>();
		
		vehicles.add(new Car());
		vehicles.add(new Bus());
		vehicles.add(new EstateCar());
		
		// me teame, et k6ik on sama tyypi, aga...
		for (Vehicle vehicle : vehicles) {
			vehicle.drive(); // ..meetodi kaitumine erineb
		}
	}
}
