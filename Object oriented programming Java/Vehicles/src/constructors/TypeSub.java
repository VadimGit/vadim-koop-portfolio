package constructors;

// subclass
public class TypeSub extends TypeExample{
	
	private int age;
	private String typeLocation;
	
	public TypeSub() {
		age = 9; // algv��rtustamine
		System.out.println("argumendita SUB");
	}
	
	public TypeSub(String typeLocation) {
		this(); // argumendita konstruktori v�lja
		System.out.println("SUB");
		this.typeLocation = typeLocation;
	}
	
	@Override
	public Integer example() {
		// alamklassi meetod v6ib ka tagastatava tyybi muuta kui see on
		// yleklassi return type' alamtyyp
		return 5;
	}
	
	// overload - sama nimi, erinev argument
	public String example(Boolean b) {
		return "";
	}
	
	public void getNum(long sum) {
		
	}
	
	public void getNum(String sumStr) {
	
	}
}
