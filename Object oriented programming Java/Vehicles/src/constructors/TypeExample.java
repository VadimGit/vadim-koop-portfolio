package constructors;

// superclass
public class TypeExample {
	
	public TypeExample() {
		System.out.println("SUPER senza args");
	}
	
	public TypeExample(String typeLocation) {
		System.out.println("SUPER");
	}
	
	// vaadake, milline on alamklassis
	// ylekirjutatud meetodi return type
	public Number example() {
		Number n = new Integer(5);
		return n;
	}
}
