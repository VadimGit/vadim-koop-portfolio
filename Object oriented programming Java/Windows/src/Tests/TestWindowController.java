package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import windows.RearWindow;
import windows.Window;
import windows.WindowController;

public class TestWindowController {

	@Test
	public void testGetWindow() {
		WindowController wc = new WindowController();
		assertTrue(wc.getWindow("A") instanceof Window);
		assertTrue(wc.getWindow("B") instanceof Window);
		//we check that wc.getWindow("C")
		//returns an object of sub-class of class Window
		assertTrue(Window.class.isInstance(wc.getWindow("C")));
		assertTrue(wc.getWindow("D") instanceof RearWindow);
	}
	
	
	//Test
	public void testWindowBlockInitialState(){
		WindowController wc = new WindowController();
		assertFalse(wc.windowsBlocked());
	}
	
	//Test
	public void testWindowBlock(){
		WindowController wc = new WindowController();
		wc.blockRearWindows();
		wc.allowRearWindows();
		assertTrue(wc.windowsBlocked());
	}
	
	//Test
	public void testWindowUnBlock(){
		WindowController wc = new WindowController();
		wc.blockRearWindows2();
		wc.allowRearWindows2();
		assertTrue(wc.windowsBlocked());
	}
}
