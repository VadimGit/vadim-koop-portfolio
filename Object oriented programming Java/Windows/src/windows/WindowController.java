package windows;

public class WindowController {
	
	private static Button b = new Button();
	
	public static Window getWindow(String name){
		return (name.equals("C") || name.equals("D")) ?
				new RearWindow(name,b) : new Window(name,b);
	}
	
	public void allowRearWindows(){
		Window.allowRearWindows = true;
		
	}
	
	public void allowRearWindows2(){
		b.enableWindows();
	}

	public void blockRearWindows2(){
		b.disableWindows();
	}
	
	public boolean windowsBlocked(){
		return !Window.allowReareWindows;  //! otricanie
	}

	public void blockRearWindows() {
		// TODO Auto-generated method stub
		
	}
}
