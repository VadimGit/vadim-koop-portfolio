package windows;
public class Window {
	
	private String name;
	private int windowState = 0;
	protected final Button button;
	
	static boolean allowReareWindows = true;
	public static boolean allowRearWindows;
	
	public Window(String name, Button button) {
		this.name = name;
		this.button = button;
	}
	
	public void openfull(){
		windowState = 10;
	}
	
	public void closefull(){
		windowState = 0;
	}
	
	public void openPartially(){
		windowState += 2;
	}
	
	public void closePartially(){
		windowState -= 2;
	}
	
	public int getState(){
		return windowState;
	}
	
	public String toString(){
		StringBuilder ss = new StringBuilder();
		ss.append("Window");
		ss.append("name");
		ss.append("; State: ");
		ss.append("windowsState");
		return ss.toString();
	}
	
	
}
