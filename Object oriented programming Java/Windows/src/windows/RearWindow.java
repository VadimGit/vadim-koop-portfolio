package windows;

public class RearWindow extends Window {
	
	public RearWindow(String name, Button button) {
		super(name, button);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void openfull() {  // ctr + space
		if(button.IsWindowsEnabled()){
			super.openfull();
		} else{
			System.out.println("Windows cannot be operated at the moment");
		}
		
	} 
	
	@Override
	public void closefull() {
		if(button.IsWindowsEnabled()){
			super.closefull();
		} else{
			System.out.println("Windows cannot be operated at the moment");
		}
		
	}
	
	@Override
	public void openPartially() {
		if (Window.allowReareWindows){
			super.openPartially();
		} else{
			System.out.println("Windows cannot be operated at the moment");
		}
		
	}
	
	@Override
	public void closePartially() {
		if(Window.allowReareWindows){
			super.closePartially();
		}else{
			System.out.println("Windows cannot be operated at the moment");
		}
		
	}

}
