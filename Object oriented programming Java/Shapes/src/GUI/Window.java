package GUI;
/** @author Oleg Toming 083905 IAPB48
 *  @version 1.0
 ** Kujundid
 *  Window Class
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import figures.*;

public class Window extends JFrame {
	/** @param TOP - number of tops (vershini)
	 *  @param base - JFrame
	 *  @param control, drawArea - JPanels
	 *  @param grid, star, ornament - kujundid
	 *  @param tee - teehoiumaks
	 */
	private static final long serialVersionUID = 1L;
	
    private static final int TOP = 5;

    private JFrame base = new JFrame("Kujundid by Oleg Toming ver 1.3.3.7");

    private Container pane = base.getContentPane();

    private JPanel control = new JPanel(new FlowLayout());
    private JPanel drawArea = new JPanel(new FlowLayout());

    private JLabel midPoint = new JLabel("Center");
    private JLabel x = new JLabel("X:");
    private JLabel y = new JLabel("Y:");
    
    private JTextField xPoint = new JTextField("0", 3);
    private JTextField yPoint = new JTextField("0", 3);

    private JButton setData = new JButton("Change");
    public static Container container;

    
    Kujund grid = new Grid(200, 75, 100, TOP);
    Kujund star = new Star(200, 75, 100, TOP);
    Kujund ornament = new Ornament(200, 75, 100, TOP);
    

    public Window(String name, int width, int height) {
    	/** new Window
    	 */
        base.setName(name);
        base.setSize(width, height);
        base.setLocationRelativeTo(null);
        base.setDefaultCloseOperation(EXIT_ON_CLOSE);

        pane.setLayout(new BorderLayout());

        control.add(midPoint);
        control.add(x);
        control.add(xPoint);
        control.add(y);
        control.add(yPoint);
        control.add(setData);
        
        setData.addActionListener(new BL());
        
        drawArea.add(grid, FlowLayout.LEFT);
        drawArea.add(star, FlowLayout.CENTER);
        drawArea.add(ornament, FlowLayout.RIGHT);
        
        pane.add(drawArea, BorderLayout.CENTER);

        pane.add(control, BorderLayout.NORTH);
        base.setForeground(Color.GRAY);
       // base.setResizable(false);
        base.setVisible(true);
   

    }
    
    class BL implements ActionListener {
    	/** ActionListener
    	 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
			grid.setMidPoint(Integer.parseInt(xPoint.getText()), Integer.parseInt(yPoint.getText()));
			star.setMidPoint(Integer.parseInt(xPoint.getText()), Integer.parseInt(yPoint.getText()));
			ornament.setMidPoint(Integer.parseInt(xPoint.getText()), Integer.parseInt(yPoint.getText()));
			drawArea.repaint();
			drawArea.validate();
			} catch(NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Wrong data!");
			}
		}
    	
    }
    
}

