package figures;
/** @author Oleg Toming 083905 IAPB48
 *  @version 1.0
 ** Kujundid
 *  Point Class
 */
public class Point {
	/** @param x,y  - koordinats
	 */
    private int x;
    private int y;
    
    Point(double x, double y) {
        this.x = (int) Math.round(x);
        this.y = (int) Math.round(y);
    }

    public int getX() {
    	/** @return x
		 */
        return x;
    }

    public int getY() {
    	/** @return y
		 */
        return y;
    }
    public Point add(Point p) {
        return new Point(this.x + p.getX(), this.y + p.getY());
    }
}
