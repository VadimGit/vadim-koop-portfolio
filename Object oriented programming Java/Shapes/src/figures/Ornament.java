package figures;
/** @author Oleg Toming 083905 IAPB48
 *  @version 1.0
 ** Kujundid
 *  Ornament Class
 */
import java.awt.Graphics;

public class Ornament extends Kujund {
    private static final long serialVersionUID = 1L;

    public Ornament(int x, int y, int radius, int points) {
        super(x, y, radius, points);
    }

    @Override
    protected void drawKujund(Point p, Graphics g) {
    	/** draw Ornament
    	 */
        Point mid = this.getMidPoint();
        g.drawLine(p.getX()+midX, p.getY()+midY, mid.getX(), mid.getY());
        
        count++;
        cycle++;
        
        
        if (count == 2) {
            lastPoint = p;
            if (cycle == nrOfPoints)
                g.drawLine(p.getX()+midX, p.getY()+midY, firstPoint.getX(), firstPoint.getY());
        }
        else if (count == 3) {
            g.drawLine(p.getX()+midX, p.getY()+midY, lastPoint.getX()+midX, lastPoint.getY()+midY);
            lastPoint = null;
            count -= 2;
        }
    }

}

