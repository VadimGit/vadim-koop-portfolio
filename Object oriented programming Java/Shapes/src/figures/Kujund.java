package figures;
/** @author Oleg Toming 083905 IAPB48
 *  @version 1.0
 ** Kujundid
 *  Kujund Class
 */
import java.awt.*;
import java.util.Vector;
import javax.swing.JPanel;

public abstract class Kujund extends JPanel {
	/** @param midPoint - middle of figure
	 *  @param angle - ugol
	 *  @param points - Vector of points
	 *  @param firstPoint - start of draw
	 *  @param lastPoint - end of draw
	 *  @param nrOfPoints - how many points
	 *  @param count - how many
	 *  @param cycle - how many cycles
	 */
	private static final long serialVersionUID = 1L;
	private Point  midPoint;
	private double angle;
	private static Vector<Point> points = new Vector<Point>();
	protected Point firstPoint;
	protected Point lastPoint;
	protected int   nrOfPoints;
	protected int   count;
	protected int   cycle;
	
	protected int midX;
	protected int midY; 
	abstract protected void drawKujund(Point p, Graphics g);
	
	
     Kujund(int x, int y, int radius, int points) {
		/** new Kujund
    	 */
		
		setSize(400, 400);
		setPreferredSize(getSize());
		setBackground(Color.WHITE);
		setFirstPoint(x, y);
		setMidPoint(x, y, radius);
		setNrOfPoint(points);
		setAngle(points);
		
		joonistaKujund(midPoint.getX(), midPoint.getY(), nrOfPoints, angle, radius);
	}
 	public final void paintComponent (Graphics g)
	  { 
		super.paintComponent(g);
		 init();
			for (Point p : points) {
				drawKujund(p, g);
			}
		
		}

	final protected void joonistaKujund(double midX, double midY, int n, double angle, int radius) {
		/** draw Kujund
    	 */
		if (points.isEmpty()) {
		double alpha = 3 * Math.PI / 2;
		for (int i = 0; i < n; i++) {
			points.add(new Point(Math.round(radius * Math.cos(alpha)), Math.round(radius * Math.sin(alpha))));
			alpha -= angle;
			for (Point p : points) {
				drawKujun(p);
			}
			
		}
		}
	}
	

	public Point getMidPoint() {
		/** @return midPoint
		 */
		return midPoint;
	}
	
	public void setMidPoint(int x, int y) {
		 midPoint = new Point(x, y);
	}

	protected void setFirstPoint(int x, int y) {
		firstPoint = new Point(x, y);
	}

	final protected void setMidPoint(int x, int y, int radius) {
		midPoint = new Point(x, y + radius);
	}

	final protected void setNrOfPoint(int points) {
		this.nrOfPoints = points;
	}

	final protected void setAngle(int points) {
		this.angle = 2 * Math.PI / points;
	}

	final public Vector<Point> getPoints() {
		/** @return points
		 */
		return points;
	}
	
	protected void drawKujun(Point p){};

	final private void init() {
		lastPoint = null;
		count = 0;
		cycle = 0;
		midX = midPoint.getX();
		midY = midPoint.getY();
		firstPoint = new Point(points.get(0).getX() + midX, midY + points.get(0).getY());
	}
}

