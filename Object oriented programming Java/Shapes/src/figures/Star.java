package figures;
/** @author Oleg Toming 083905 IAPB48
 *  @version 1.0
 ** Kujundid
 *  Star Class
 */
import java.awt.*;

public class Star extends Kujund {
    private static final long serialVersionUID = 1L;

    public Star(int x, int y, int radius, int points) {
        super(x, y, radius, points);
    }

    @Override
    protected void drawKujund(Point p, Graphics g) {
    	/** draw Star
    	 */
        Point mid = this.getMidPoint();
        g.setColor(Color.BLACK);
        g.drawLine(p.getX()+midX, p.getY()+midY, mid.getX(), mid.getY());
    }

}

