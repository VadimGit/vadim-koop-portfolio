package figures;
/** @author Oleg Toming 083905 IAPB48
 *  @version 1.0
 ** Kujundid
 *  Grid Class
 */
import java.awt.*;

final public class Grid extends Kujund {
    private static final long serialVersionUID = 1L;

    public Grid(int x, int y, int radius, int points) {
        super(x, y, radius, points);
    }

    @Override
    protected void drawKujund(Point p, Graphics g) {
    	/** draw Grid
    	 */
        if (lastPoint == null)
            lastPoint = p;
        else {
            g.setColor(Color.BLACK);
            g.drawLine(lastPoint.getX()+midX, lastPoint.getY()+midY, p.getX()+midX, p.getY()+midY);
            lastPoint = p;
        }
        
        cycle++;
        
        if (cycle == nrOfPoints)
        	g.drawLine(firstPoint.getX(), firstPoint.getY(), p.getX()+midX, p.getY()+midY);
    }
}

