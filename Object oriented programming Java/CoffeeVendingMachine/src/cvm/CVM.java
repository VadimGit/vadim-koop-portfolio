package cvm;

public class CVM {
	
	protected float diameter;
	private int counter = 0;
	
	public CVM() {
		diameter = 1;
	}
	
	public float getDiameter() {
		return diameter;
	}
	
	public int getCounter() {
		return 0;
	}
	
	public void clean(){
		
	}
	
	public void economMode(int min) throws IllegalArgumentException {
		throw new NullPointerException();
	}
	
	public boolean isEconomMode() {
		return true;
	}
	
	public void quitEconomMode() {
		
	}
	
	public void giveMeCoffeeNow() {
		++counter;
	}
}
