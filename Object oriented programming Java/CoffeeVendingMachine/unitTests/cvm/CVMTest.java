package cvm;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

public class CVMTest {

	@Test
	public void testGetDiameter() {
		assertTrue( new CVM().getDiameter() == 1 );
		assertTrue( new EspressoVM().getDiameter() == (float) 0.3 );
	}

	@Test
	public void testGetCounter() {
		CVM m1 = new CVM();
		EspressoVM m2 = new EspressoVM();
		int z = new Random().nextInt(100)+1;
		
		for(int i=0; i<z; z++) {
			m1.giveMeCoffeeNow();
			m2.giveMeCoffeeNow();
		}
		
		assertTrue( z == m1.getCounter() );
		assertTrue( z == m2.getCounter() );
	}

	@Test
	public void testClean() {
		fail("Not yet implemented");
	}

	@Test
	public void testEconomMode() {
		CVM m1 = new CVM();
		EspressoVM m2 = new EspressoVM();
		m1.isEconomMode();
		m2.isEconomMode();
	}

	@Test
	public void testIsEconomMode() {
		fail("Not yet implemented");
	}

	@Test
	public void testQuitEconomMode() {
		fail("Not yet implemented");
	}

}
