package ee.ttu.algoritmid.labyrinth;
/**
 * Vadim Maze HW03!
 */
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class HW03 {
    public MazeRunner mazeRunner;
    public SimpleEntry<Integer, Integer> location;
    public SimpleEntry<Integer, Integer> thisLocation = new SimpleEntry<>(Integer.MAX_VALUE, Integer.MAX_VALUE);
    public Integer goodStepValueChoice = Integer.MAX_VALUE;
    public List<List<Integer>> gSearch;
    public int goodMovementValue;
    public int[][] theMazeMap;
    public List<SimpleEntry<List<String>, Integer>> paths;

    /**
     * HW03 method/
     * @param fileName String/
     * @throws IOException IOException.
     * @throws URISyntaxException URISyntaxException.
     */
    public HW03(String fileName) throws IOException, URISyntaxException {
        mazeRunner = new MazeRunner(fileName);
    }

    /**
     * MazeRunner getMazeRunner() method.
     * @return
     */
    public MazeRunner getMazeRunner() {
        return mazeRunner;
    }


    /**
     * List<String> solve() method.
     * @returnsteps which is done as the list of strings else null.
     */
    public List<String> solve() {
        Stack<String> thisDirection = new Stack<>();
        List<String> bestestDirection = new ArrayList<>();
        location = mazeRunner.getPosition();
        this.paths = new ArrayList<>();
        SimpleEntry<Integer, Integer> mapSize = mazeRunner.getSize();
        theMazeMap = new int[mapSize.getKey() + 2][mapSize.getValue() + 2];
        for (int aCoordinate = 0; aCoordinate < mapSize.getKey() + 2; ++aCoordinate) {
            for (int bCoordinate = 0; bCoordinate < mapSize.getValue() + 2; ++bCoordinate) {
                if (!(mazeRunner.getPosition().getValue() == bCoordinate - 1) || !(mazeRunner.getPosition().getKey() == aCoordinate - 1))
                    theMazeMap[aCoordinate][bCoordinate] = Integer.MAX_VALUE;
                else
                    theMazeMap[aCoordinate][bCoordinate] = 0;}}
        String thisMove;
        do {
            thisMove = pickDirection(thisDirection);
            while(!thisDirection.isEmpty() && thisMove.equals("")) {
                directionToBack(thisDirection);
                thisMove = pickDirection(thisDirection);}
            if (!thisMove.equals(""))
                movementToTheWay(thisDirection, thisMove);
        } while (!thisMove.equals("") || !thisDirection.isEmpty());
        if (this.paths.isEmpty())
            return null;
        Integer bestestValue = Integer.MAX_VALUE;
        for (SimpleEntry<List<String>, Integer> entry : this.paths) {
            if (entry.getValue() < bestestValue) {
                bestestDirection = entry.getKey();
                bestestValue = entry.getValue();}}
        return bestestDirection;}

    /**
     * pickDirection method.
     * @param thePath Stack<String>.
     * @return movement.
     */
    public String pickDirection(Stack<String> thePath) {
        String endTheStep = "", walking = "";
        if (!thePath.isEmpty())
            endTheStep = thePath.peek();
        goodMovementValue = Integer.MAX_VALUE;
        location = mazeRunner.getPosition();
        int thisLocationValue = theMazeMap[location.getKey() + 1][location.getValue() + 1];
        if (goodStepValueChoice < Integer.MAX_VALUE) {
            int length = Math.abs(thisLocation.getValue() - location.getValue() + Math.abs(thisLocation.getKey() - location.getKey() + 1) + 1);
            if (thisLocationValue + length >= goodStepValueChoice)
                return "";}
        gSearch = mazeRunner.scan();
        switch (endTheStep) {
            case "E": walking = keepMoving(endTheStep, thisLocationValue, 1, 0);
                break;
            case "N": walking = keepMoving(endTheStep, thisLocationValue, 0, 1);
                break;
            case "S": walking = keepMoving(endTheStep, thisLocationValue, 2, 1);
                break;
            case "W": walking = keepMoving(endTheStep, thisLocationValue, 1, 2);
                break;
        }
        if (walking.equals("")) {
            if (!endTheStep.equals("N"))
                walking = pickTheNewMovement("S", thePath, thisLocationValue, 2, 1, walking);
            if (!endTheStep.equals("W"))
                walking = pickTheNewMovement("E", thePath, thisLocationValue, 1, 2, walking);
            if (!endTheStep.equals("E"))
                walking = pickTheNewMovement("W", thePath, thisLocationValue, 1, 0, walking);
            if (!endTheStep.equals("S"))
                walking = pickTheNewMovement("N", thePath, thisLocationValue, 0, 1, walking);
        }
        return walking;
    }

    /**
     * pickTheNewMovement method.
     * @param lastStep String.
     * @param way Stack<String>.
     * @param thisLocatinValue locationPeremennaja.
     * @param a int.
     * @param b int.
     * @param step String.
     * @return
     */
    public String pickTheNewMovement(String lastStep, Stack<String> way, int thisLocatinValue, int a, int b, String step) {
        Integer thisMapValue = theMazeMap[location.getKey() + b][location.getValue() + a];
        Integer searchThisValue = thisLocatinValue + gSearch.get(a).get(b);
        if (thisMapValue <= searchThisValue || gSearch.get(a).get(b) == -1)
            return step;
        else if (gSearch.get(a).get(b) == -2) {
            List<String> lastMovement = new ArrayList<>(way);
            lastMovement.add(lastStep);
            SimpleEntry<List<String>, Integer> stopMovement = new SimpleEntry<>(lastMovement, thisLocatinValue);
            this.paths.add(stopMovement);
            if (goodStepValueChoice == Integer.MAX_VALUE)
                thisLocation = new SimpleEntry<>(location.getKey() + b, location.getValue() + a);
            if (thisLocatinValue < goodStepValueChoice)
                goodStepValueChoice = thisLocatinValue;
        }
        else if (searchThisValue < goodStepValueChoice && searchThisValue < goodMovementValue) {
            goodMovementValue = gSearch.get(a).get(b);
            return lastStep;}
        return step;}

    /**
     * keepMoving method.
     * @param latestStep String.
     * @param whichLocation int.
     * @param a int.
     * @param b int.
     * @return
     */
    public String keepMoving(String latestStep, int whichLocation, int a, int b) {
        int mapPosition, searchPosition;
        String step = "";
        if (gSearch.get(a).get(b) > 0) {
            mapPosition = theMazeMap[location.getKey() + b][location.getValue() + a];
            searchPosition = gSearch.get(a).get(b) + whichLocation;
            if (mapPosition > searchPosition)
                step = latestStep;}
        return step;
    }

    /**
     * movementToTheWay method.
     * @param theMazeWay Stack<String>.
     * @param theMazeDerection String.
     */
    public void movementToTheWay(Stack<String> theMazeWay, String theMazeDerection) {
        if (theMazeDerection.equals("S") || theMazeDerection.equals("N") || theMazeDerection.equals("E") || theMazeDerection.equals("W")) {
            int movementValue = theMazeMap[location.getKey() + 1][location.getValue() + 1];
            mazeRunner.move(theMazeDerection);
            theMazeWay.push(theMazeDerection);
            if (theMazeDerection.equals("W"))
                theMazeMap[location.getKey()][location.getValue() + 1] = movementValue + gSearch.get(1).get(0);
            if (theMazeDerection.equals("E"))
                theMazeMap[location.getKey() + 2][location.getValue() + 1] = movementValue + gSearch.get(1).get(2);
            if (theMazeDerection.equals("N"))
                theMazeMap[location.getKey() + 1][location.getValue()] = movementValue + gSearch.get(0).get(1);
            if (theMazeDerection.equals("S"))
                theMazeMap[location.getKey() + 1][location.getValue() + 2] = movementValue + gSearch.get(2).get(1);
            location = mazeRunner.getPosition();
        }
    }

    /**
     * directionToBack method.
     * @param way Stack<String>.
     */
    public void directionToBack(Stack<String> way) {
        if (way.peek().equals("S"))
            mazeRunner.move("N");
        else if (way.peek().equals("W"))
            mazeRunner.move("E");
        else if (way.peek().equals("E"))
            mazeRunner.move("W");
        else if (way.peek().equals("N"))
            mazeRunner.move("S");
        else
            return;
        way.pop();
        location = mazeRunner.getPosition();
    }

    /**
     * Main method.
     * @param args args.
     * @throws IOException IOException.
     * @throws URISyntaxException URISyntaxException.
     */
    public static void main(String[] args) throws IOException, URISyntaxException {
        HW03 hw03 = new HW03("maze.txt");
        System.out.println(hw03.solve());
    }
}