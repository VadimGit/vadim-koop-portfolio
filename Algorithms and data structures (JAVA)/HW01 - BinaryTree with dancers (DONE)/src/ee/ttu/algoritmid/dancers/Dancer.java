package ee.ttu.algoritmid.dancers;
/**
 * Objects for dancers....
 */
public interface Dancer {
    public enum Gender {
        MALE, FEMALE
    }
    public int getID();
    public Gender getGender();
    public int getHeight();
}