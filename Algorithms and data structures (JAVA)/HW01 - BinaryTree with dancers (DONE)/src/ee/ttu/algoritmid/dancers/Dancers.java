package ee.ttu.algoritmid.dancers;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;
/**
 * Interface.
 */
/**
 * Returns waiting list as list for man and woman. (shortest to longest)
 * In case if W and M have same height, order must be W, M....
 */
public interface Dancers {
    public SimpleEntry<Dancer, Dancer> findPartnerFor(Dancer d) throws IllegalArgumentException;
    public List<Dancer> returnWaitingList();
}