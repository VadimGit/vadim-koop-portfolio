package ee.ttu.algoritmid.dancers;

import static ee.ttu.algoritmid.dancers.Dancer.Gender.FEMALE;

public class Node {

    private int id;

    private Dancer dancer = null;

    public Dancer getDancer() {
        return dancer;
    }

    private Node parent = null;
    private Node leftChild = null;
    private Node rightChild = null;

    public Node(Dancer dancer) {  //if we X2 always paaris arv.
        int genderBit = (dancer.getGender().equals(FEMALE)) ? 1 : 0; //if CONDITION? true : false
        this.setId(dancer.getHeight()*2 + genderBit); // female paaritu (genderBit+ if it woman) (if man= paaris)
        this.dancer = dancer; // 150 male, 150 female, ...150*2 + 0 = 300, 150*2 + 1 = 301, ...300, 301, ...
    }

    public String toString() {
        return String.format(
                "Node %d: (%d %s, %d)",  //d = 4islo, s = String..
                getId(),
                dancer.getHeight(), //rost
                dancer.getGender().name().charAt(0), // first letter of the sex
                dancer.getID()
        );
    }

    /*
     * Getters and Setters!
     */
    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}