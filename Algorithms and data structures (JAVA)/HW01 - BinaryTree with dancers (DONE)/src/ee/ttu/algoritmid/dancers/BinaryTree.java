package ee.ttu.algoritmid.dancers;

import java.util.List;

import static ee.ttu.algoritmid.dancers.Dancer.Gender.MALE;
import static ee.ttu.algoritmid.dancers.Dancer.Gender.FEMALE;

public class BinaryTree {

    private Node root = null; // always null at this moment

    public Node add (Dancer dancer) {

        Node node = new Node(dancer); //add dancer, we create node.(IN Tree)
        if (root == null) {
            root = node;
            return node;
        }

        Node parent; // creating parent, mis on null alguses.
        Node current = root; // starting with root checking

        while (true) {
            parent = current; //Keep parent for reference

            if (node.getId() < current.getId()) { //if we sent it to left
                current = current.getLeftChild(); // trying go down trough tree

                if (current == null) { // if we get to the end of the tree!
                    parent.setLeftChild(node);
                    node.setParent(parent);
                    return node;
                }

            } else { // if we sent it to right
                current = current.getRightChild(); // trying go down trough tree

                if (current == null) { // if we get to the end of the tree!
                    parent.setRightChild(node);
                    node.setParent(parent);
                    return node;
                }
            }
        }
    }

    public Node findPartner(Node node) { //zensina sprava, muztsina sleva (if rost == rost)

        if (node.getDancer().getGender() == MALE) {
            Node partner = findMinHeight(node);

            if (partner == null) {
                return null;
            }

            if (partner.getDancer().getGender() == FEMALE) { //check that female
                return partner;
            }

        } else if (node.getDancer().getGender() == FEMALE) {
            Node partner = findMaxHeight(node);

            if (partner == null) {
                return null;
            }

            if (partner.getDancer().getGender() == MALE) { //check that male
                return partner;
            }
        }

        return null;  // if fail 56 or 67 returns null
    }

    public Node findMaxHeight(Node node) { //zensina viwe

        if (node.getParent().getLeftChild() == node) { // checking if node is left child
            Node found = node.getParent();

            if (found.getDancer().getGender() == MALE) { //if parent = male, we return it.
                return found;
            }

            return null;
//parent bolwe nas
        } else if (node.getParent().getRightChild() == node) { // checking if node is right child/
            Node found = node.getParent();
            found = getMinNode(found); //search the min node of our parent

            if (found.getParent() != null) { // if found not root  (ROOT do not have parent)
                found = found.getParent(); //going up on one level

                if (found.getDancer().getGender() == MALE) { // and if it male -> return
                    return found;
                }
            }

            return null;
        }

        return null;
    }

    public Node findMinHeight(Node node) {  //Same thing, but reverse!

        if (node.getParent().getRightChild() == node) {
            Node temporary = node.getParent();

            if (temporary.getDancer().getGender() == FEMALE) {
                return temporary;
            }

            return null;


        } else if (node.getParent().getLeftChild() == node) {

            Node temporary = node.getParent();
            temporary = getMaxNode(temporary);

            if (temporary.getParent() != null) {
                temporary = temporary.getParent();

                if (temporary.getDancer().getGender() == FEMALE) {
                    return temporary;
                }
            }

            return null;
        }

        return null;
    }

    public boolean remove(Node node) {
        Node parent = node.getParent();

        boolean isLeftChild = (parent != null && parent.getLeftChild() == node); // check are we left

        if (node.getLeftChild() == null && node.getRightChild() == null) { // if do not have children

            if (node == root) { //if root than we delete it
                root = null;
                return true;
            }

            if (isLeftChild) {
                parent.setLeftChild(null);  //parent do not have left child
                node.setParent(null); //node none parents
            } else {
                parent.setRightChild(null);
                node.setParent(null);
            }
        }

        else if (node.getRightChild() == null) { //only one left child, none right

            if (node == root) {
                root = node.getLeftChild(); // if we root, we replace to the left child
                root.setParent(null);
            } else if (isLeftChild) { // link
                parent.setLeftChild(node.getLeftChild());
                node.getLeftChild().setParent(parent);
                node.setLeftChild(null);
                node.setParent(null);
            } else {
                parent.setRightChild(node.getLeftChild());
                node.getLeftChild().setParent(parent);
                node.setLeftChild(null);
                node.setParent(null);
            }

        } else if (node.getLeftChild() == null) { // if left none, only right is.

            if (node == root) { // same operation
                root = node.getRightChild();
                root.setParent(null);
            } else if (isLeftChild) {
                parent.setLeftChild(node.getRightChild());
                node.getRightChild().setParent(parent);
                node.setRightChild(null);
                node.setParent(null);
            } else {
                parent.setRightChild(node.getRightChild());
                node.getRightChild().setParent(parent);
                node.setParent(null);
                node.setRightChild(null);
            }

        } else if (node.getLeftChild() != null && node.getRightChild() != null) { // delete node with 2 children
            Node replacement = getReplacementForNode(node);

            if (node == root) {
                root = replacement;
                root.setParent(null);
            } else if (isLeftChild) {
                replacement.setParent(node.getParent()); //replace parent to new one (link)
                parent.setLeftChild(replacement);
            } else { // same thing if we right child
                replacement.setParent(node.getParent());
                parent.setRightChild(replacement);
            }

            if (node.getLeftChild() != null) { // set new parents to the nodes. (LEFT CHILD)
                node.getLeftChild().setParent(replacement);
            }

            if (node.getRightChild() != null && (replacement != node.getRightChild())) { //(RIGHT CHILD)   DO NOT SET PARENT iseendale
                node.getRightChild().setParent(replacement);
            }

            replacement.setLeftChild(node.getLeftChild()); //replace left child
            node.setLeftChild(null);
            node.setRightChild(null);  // cutting old node, which I deleted. NONE CHILDREN NOW
        }

        return true;
    }

    public Node getReplacementForNode(Node node) {
        Node replacement = null;
        Node parent = null;
        Node current = node.getRightChild(); // start from right child

        while (current != null) { // We going to LEFT as possible
            parent = replacement;
            replacement = current;
            current = current.getLeftChild();
        }

        if (replacement != node.getRightChild()) {  // if is there is none direct link
            parent.setLeftChild(replacement.getRightChild());

            if (replacement.getRightChild() != null) { // replace as we got 1 child
                replacement.getRightChild().setParent(replacement.getParent());
            }

            replacement.setRightChild(node.getRightChild());
        }

        return replacement;
    }

    public Node getMaxNode(Node node) {  // going up trough left CHILDS

        while (node.getParent() != null && node == node.getParent().getLeftChild()) {
            node = node.getParent();
        }

        return node;
    }

    public Node getMinNode(Node node) { // going up trough right CHILDS

        while (node.getParent() != null && node == node.getParent().getRightChild()) {
            node = node.getParent();
        }

        return node;
    }

    public List<Dancer> getWaitingList(Node root, List<Dancer> dancers) {  // recursive format.

        if (root.getLeftChild() != null) {
            getWaitingList(root.getLeftChild(), dancers);
        }

        dancers.add(root.getDancer());

        if (root.getRightChild() != null) {
            getWaitingList(root.getRightChild(), dancers);
        }

        return dancers;
    }

    public Node getRoot() {
        return root;
    }
}

//https://neerc.ifmo.ru/wiki/index.php?title=%D0%94%D0%B5%D1%80%D0%B5%D0%B2%D0%BE_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0,_%D0%BD%D0%B0%D0%B8%D0%B2%D0%BD%D0%B0%D1%8F_%D1%80%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F#.D0.9D.D0.B5.D1.80.D0.B5.D0.BA.D1.83.D1.80.D1.81.D0.B8.D0.B2.D0.BD.D0.B0.D1.8F_.D1.80.D0.B5.D0.B0.D0.BB.D0.B8.D0.B7.D0.B0.D1.86.D0.B8.D1.8F
//http://algorithms.tutorialhorizon.com/binary-search-tree-complete-implementation/