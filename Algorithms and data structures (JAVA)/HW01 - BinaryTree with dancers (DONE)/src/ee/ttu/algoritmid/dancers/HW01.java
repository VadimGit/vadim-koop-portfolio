package ee.ttu.algoritmid.dancers;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ee.ttu.algoritmid.dancers.Dancer.Gender.FEMALE;
import static ee.ttu.algoritmid.dancers.Dancer.Gender.MALE;

public class HW01 implements Dancers {

    public BinaryTree tree = new BinaryTree();

    public SimpleEntry<Dancer, Dancer> findPartnerFor(Dancer dancer) throws IllegalArgumentException {

        if (dancer == null) {
            throw new IllegalArgumentException();
        }

        Node partner;
        Node startTheParty;

        try {
            partner = tree.add(dancer);
            startTheParty = tree.findPartner(partner);
        } catch (NullPointerException e) {
            return null;
        }

        if (startTheParty != null) {
            tree.remove(partner);
            tree.remove(startTheParty);

            if (startTheParty.getDancer().getGender().equals(FEMALE)) {
                return new SimpleEntry <> (startTheParty.getDancer(), dancer);
            } else if (startTheParty.getDancer().getGender().equals(MALE)) {
                return new SimpleEntry <> (dancer, startTheParty.getDancer());
            }
        }

        return null;
    }

    public List<Dancer> returnWaitingList() {//.

        try {
            List<Dancer> list = new ArrayList <>();
            tree.getWaitingList(tree.getRoot(), list);
            Collections.sort(list, new Comparator<Dancer>() {

                public int compare(Dancer dancers1, Dancer dancers2) {
                    if (dancers1.getHeight() == dancers2.getHeight() && dancers1.getGender() == FEMALE) {
                        return -1;
                    } else if (dancers1.getHeight() == dancers2.getHeight()) {
                        return 0;
                    }
                    return dancers1.getHeight() > dancers2.getHeight() ? 1 : -1;//.
                }
            });
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }
}