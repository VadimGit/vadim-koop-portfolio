package ee.ttu.algoritmid.tsp;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Main Static.
 */
public class TSP {

	public static void main(String[] args) throws Exception {
		// Test
	}
	Counter counter = new Counter();
	DepthFirstSolver depth = new DepthFirstSolver();

	/* Depth first search... */
	public List<Integer> depthFirst(int[][] adjacencyMatrix) {
		// Control the MATRIX. Kiired lahendused!
		if(adjacencyMatrix.length == 0 || adjacencyMatrix[0].length == 0) return new ArrayList<>();
		if(adjacencyMatrix.length == 1) {
			return Arrays.asList(0); //list of one town 0.
		}

		counter = new Counter();
		return depth.getSolution(adjacencyMatrix, counter);
	}

	/* Best first search. */
	public List<Integer> bestFirst(int[][] adjacencyMatrix) {

		if(adjacencyMatrix.length == 0 || adjacencyMatrix[0].length == 0) return new ArrayList<>();
		if(adjacencyMatrix.length == 1) {
			return Arrays.asList(0);
		}
		return null;
	}

	/* Nodes viewed in last matrix to find the solution (should be zeroed at the beginning of search) */
	public BigInteger getCheckedNodesCount() {
		return counter.getCount();
	}
}