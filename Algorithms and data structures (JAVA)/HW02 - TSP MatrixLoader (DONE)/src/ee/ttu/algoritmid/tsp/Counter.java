package ee.ttu.algoritmid.tsp;

import java.math.BigInteger;

/**
 * In task we supposed to write how many paths have checked!!!
 * This is the count, which is +ONE(increment function) to the current number!
 */
public class Counter {

	private BigInteger counter = BigInteger.ZERO;

	public void increment() {
		counter = counter.add(BigInteger.ONE);
	}

	public BigInteger getCount() {
		return counter;
	}
}
