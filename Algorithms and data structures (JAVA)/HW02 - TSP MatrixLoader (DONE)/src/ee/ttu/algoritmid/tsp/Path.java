package ee.ttu.algoritmid.tsp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The main file of our algorithm!
 * This class describes our way! In stead of STACK! This whay of solution is more convenient!
 */
public class Path {

	private int[][] adj;  //Remember Matrix
	private int numberOfCities;
	private List<Integer> cities; // which path already used!
	private Set<Integer> candidates; //Avoids the duplicates!
	private long cost;

	/**
	 * Constructor.
	 * Here we copy the matrix from DFS. It's means that we have correct details. It do need it validate again.
	 */
	public Path(int[][] adj) {  //A4 -> 7
		this.adj = adj; // give as value
		this.numberOfCities = adj.length;
		this.cities = new ArrayList<>();
		this.cities.add(0);
		this.candidates = new HashSet<>(numberOfCities);
		for(int i = 1; i < numberOfCities; i++) {
			this.candidates.add(i);
		}
		this.cost = 0L;
	}
		/**
	 * Constructor for paths! Taking existing path, copy it and put,
		 * next city at the end, remove next city from future candidates, update cost.
	 */
	private Path(Path path, int nextCity) { //A4 -> 6
		this.adj = path.adj;
		this.numberOfCities = path.numberOfCities;
		this.cities = new ArrayList<>();
		this.cities.addAll(path.cities);
		this.candidates = new HashSet<>(numberOfCities);
		this.candidates.addAll(path.candidates);
		this.cities.add(nextCity);
		this.candidates.remove(nextCity);
		this.cost = path.cost + path.adj[path.getLastCity()][nextCity];
	}

	public int getLastCity() {
		return cities.get(cities.size() - 1);
	} //#49

	/**
	 * When the path is almost done and we supposed add the zero (Example STACK).
	 */
	private boolean isAlmostComplete() {
		return (cities.size() == numberOfCities);
	} //#76

	/**
	 * When we have +1 town bigger then we have! We do not check in path, it is for outside using.
	 */
	public boolean isComplete() {
		return (cities.size() == numberOfCities + 1);
	} //Watch in DepthFirstSolver A4 => 7

	/**
	 * How we generate the next way from the current location!
	 * 0 -> 1, 0 -> 2, 0 -> 3.
	 */
	public List<Path> getAllNextPaths() { //A4 -> 7 USE IT IN DepthFirstSolver
		List<Path> list = new ArrayList<>(); //create list of Path (empty)
		if(isAlmostComplete()) {
			list.add(new Path(this, 0));
		} else {
			for(int i : candidates) { //candidates - set of towns where we can go
				list.add(new Path(this, i));
			}
		}
		return list;
	}

	public Long getCost() {
		return cost;
	}

	public List<Integer> getCities() {
		return cities;
	}

	@Override
	public String toString() {
		return String.format("%d = %s", cost, cities);
	}
}
