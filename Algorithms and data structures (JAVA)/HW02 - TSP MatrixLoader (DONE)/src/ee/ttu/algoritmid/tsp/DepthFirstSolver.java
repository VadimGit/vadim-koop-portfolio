package ee.ttu.algoritmid.tsp;

import java.util.Collections;
import java.util.List;
import java.util.Stack;
/**
 * We have here next things: candidates and best
 */
public class DepthFirstSolver {

	private Stack<Path> candidates = new Stack<>(); //candidates it is Stack of paths
	private Path best = null;

	/**
	 * Here we have our MATRIX and Counter, which we are using for our TASK!
	 */
	public List<Integer> getSolution(int[][] adjacencyMatrix, Counter counter) { // give matrix and counter
		Path path = new Path(adjacencyMatrix);
		candidates.push(path);
		while(!candidates.isEmpty()) {
			path = candidates.pop(); //taking from top of the stack
			List<Path> nextPaths = path.getAllNextPaths();
			nextPaths.sort(this::comparePaths); //static reference
			Collections.reverse(nextPaths);
			for(Path next : nextPaths) { //type, name : list name
				if(best == null) {  //If we do not have best solution.
					if(next.isComplete()) {  // path class
						best = next;
					} else {
						candidates.push(next);
					}
					// helps as to avoid the completed ways or it expensive than we found.
				} else if(comparePaths(next, best) == -1) { //best != null     comparatorResult Logic: -1 a<b or 0 a=b or 1 a>b
					if (next.isComplete()) {
						best = next;
					} else {
						candidates.push(next);
					}
				}
				counter.increment(); //checked!
			}
		}
		if (best == null) return null; // This should never happen!
		return best.getCities();
	}

	public int comparePaths(Path a, Path b) {
		return a.getCost().compareTo(b.getCost());
	}
}
