HW02 Rändkaupmehe ülesanne
HW02 - Rändkaupmees

Leida optimaalne lahendus asümmetrilisele rändkaupmehe ülesandele (TSP) kärpimisega otsingumeetodil.
#Найти оптимальное решение для ассиметчиного rändkaupmehe задание (TSP) подрезки метода поиска.

Lahendada TSP ülesanne kärpimiseta tagasivõtmisega otsinguga, st täieliku otsinguga
#Решить TSP задание подрезкой tagasivõtmisega поиском, полным поиском

Võrrelda kärpimisega ja kärpimiseta lahenduse efektiivsust nii vaadatud sõlmede arvu kui ajakulu mõttes.
#Сравнить подрезкой и эффективность решение подрезкой также просмотренные число узелков как затрата мысленно процесса во времени.

----------------------------------------------------НЕ НАДО ДЕЛАТЬ-----------------------------------------------------------------------
Täispunktide saamiseks tuleb lisaks:
#Для 100% нужно сделать дополнительно:
1) Lahendada TSP ülesanne parim-enne otsinguga hargne ja kärbi (branch and bound) algoritmiga.
#1)Решить TSP задание parim-enne поиском hargne/распутывать и горностай (branch and bound) алгоритмом.
2) Leida ja implementeerida täpsemat hinnangut andev kärpimisfunktsioon ning implementeerida kõik efektiivselt ilma lisakeerukust põhjustamata.
#2)Найти и имплементировать  точную оценку данных kärpimisfunktsioon тиакже имплементировать все  эффективно с пояснением сложностей.
3) Võrrelda kõigi esitatud algoritmide efektiivsust nii vaadatud sõlmede arvu kui ajakulu mõttes.
#3) Сравнить все предоставленные алгоритмы также просмтотренное число узелков + затраты времени в памяти.
4) Probleemi peab lahendama otsingumeetodil - variantide läbivaatamisega. Algoritmis võite kasutada olemasolevaid andmestruktuure, 
neid ise implementeerima ei pea. Sisendiks on NxN maatriks, mis määrab kaugused linnade vahel. Väärtus adjacencyMatrix[i][j] on kaugus
linnast i linna j. Väljundiks on N+1 suurusega massiivis linnade järjekord, mis vastab lühimale teele. Teekonna esimene ja viimane
linn peavad kokku langema. Linnasid nummerdatakse 0 ... N-1. Sisendandmed võivad olla mittesümmeetrilised või sümmeetrilised. Liides:
#4)Проблема должна быть реша методом поиска - просмотор вариантов. Алгоритмы можите использовать имеющихся стурктыры базыданных, самим их 
#имплементировать не надо! На входе NxN матрица, которая определяет расстояние между городами. Ценность adjacencyMatrix[i][j] является расстоянием
#от города i до города j. На выходе N+1 размером в массиве очередь городов, которая отвечает на кратчайшему пути. Дорога первая и последний
#город должны сопрекасатьтся/прогибаться. Города пронумеровываются 0 ... N-1. Входящие данные могут быть не симметричные или симметричные.
----------------------------------------------------НЕ НАДО ДЕЛАТЬ-----------------------------------------------------------------------

Ülesande template

getCheckedNodesCount() peab andma otsingu jooksul leitud otsingupuu sõlmede (need mille kohta leiti kaugus algpunktist ja teekonna pikkuse 
koguhinnang) arvu. Olge valmis kaitsmisel näitama kui suur osa võimalikest sõlmedest kärbiti, st milline % sõlmedest läbiti võrreldes 
täieliku otsingu sõlmede arvuga. Täieliku puu sõlmede arvu võib leida valemiga.     
#getCheckedNodesCount() должен давать во время поиска найденный узел дерева поиска (к тем, к которым нашли дистанцию от изначальное пункат и
#оценку длинну дороги) число. Буде готовы на защите показать какую большую часть из возможных узлов обрезали, т.к % узлов пойденный по сравнению
#полными поисками число узлов. Полное дерево число узвол можно найти формулой.

TSP näide

Näiteks sellise 4x4 maatriksi korral on kogu sõlmede arv 22, kui joonisel kõik harud välja joonistada ja kõik sõlmed kokku lugeda.
#К примеру таую матрицу 4х4 = число узлов 22, если на рисунке нарисовать все ветви и сосчитать все узлы.
Kärpimisega välja arvutatavate sõlmede arv (getCheckedNodesCount) on aga 17, kui joonisel kõik kärpimata sõlmed kokku lugeda.
#Подрезкой подсчитанное число узлов (getCheckedNodesCount) = 17, если на рисунке не подрезанные улы подсчитать.

Hindamine - Оценивание

Puuduliku kärpimisega optimaalse tulemuse leidmisel kuni 5 punkti. (See osa on kohustuslik)
----------------------------------------------------НЕ НАДО ДЕЛАТЬ-----------------------------------------------------------------------
Kärpimisega sügavuti otsingu (tagasivõtmisega) algoritmiga optimaalse tulemuse leidmisel kuni 7 punkti.
#Подрезкой грубокого поиска tagasivõtmisega) алгоритмом поиска оптимального результата до 7 пунктов. 
Kärpimisega parim-enne otsingu rakendamisel kuni 9 punkti.
#Подрезкой parim-enne поиском до 9 пунктов.
Kuni 10 punkti, kui teie algoritmis on lisasid, mis võimaldavad lahendada ära suurema keerukusega ülesanded.
#До 10 баллов,  если в вашем алгоритме имеются добавления, которые могут решить задания с наибольшей сложностью.
Kaitsmisel täispunktide saamiseks tuleb esitada ka kõigi erinevate kasutatud algoritmide efektiivsuse võrdluse mõõtmise tulemused.
#При защите на полные балл нужно будет предоставить разные эффективности используемых алгоритмов с сравнение результатов. 
----------------------------------------------------НЕ НАДО ДЕЛАТЬ-----------------------------------------------------------------------


Soovitusi - Пожеланиея

Alustage täielikust otsingust, mis ei kasuta kärpimist. Veenduge, et see töötab korrektselt. Kasutage testimiseks väikseid (5-10 linna) ülesandeid.
#Начните с полгого пояска, которые не испльзуют подрезку. Убедитесь, что работает корректно. Для тестирования используйте малые (5-10 города) задания.
Lisage kärpimine ja kontrollige, et algoritm töötab korrektselt.
#Добавте подрезку и проверте, что алгоримт работает корректно.
Vihjeid algoritmi täiustamiseks.
#Ссылки алгоритма  усовершенствовать.
Kui tahate algoritmi kiiremaks teha, siis mõelge täiendavatele kärpimisvõimalustele, püüdke otsida ja leida paremaid kärpimisfunktsioone ja vaadake, et teie kärpimisfunktsiooni jm arvutamine oleks efektiivne.
#Если хотите сделать алгоритм сложней, то продумайте возможности обрезки, попробуйте найти и найти наилучшие функции и посмотреть, что ваши функции и вычисления эффективны.
Kas enne tehtud tööd annab kuidagi ära kasutada uute väärtuste arvutamisel?
#Прежде проделеланная работа даёт как нибудь использовать новые параметры при подсчитывании.
Kui lähtute linnadele vastavate ühenduste miinimumväärtustest, siis kas on võimalik kasutada mõlemaid, nii sisenevaid kui väljuvaid ühendusi (mittesümmeetrilise ülesande korral võivad need olla erinevad)? Kasutada summat?
#Если вы ищете минимальные значения для соединений для соответствующих городам, тогда можно ли использовтаь оба, также входящие и выходящие соединение (не симметричные задание, могут ли быть разные?) Использовать сумму?
Miinimumväärtusi võib arvutada ette kõigi linnade kohta või ümber arvutada sõltuvalt sellest millised linnad on läbitud.
#Минимальное значение можно найти к каждому городу или пересчитать в зависимости от того какие города были пройдены.
Milliseid andmestruktuure on otstarbekas kasutada?
#Какие структуры данных целесообразнее использовать?
Uurige assignment problem-i (AP) ja kas selle lahenduse abil on võimalik luua efektiivsem kärpimine. Nendel kahel probleemil on mitmeid seoseid ja erinevaid algoritme, mis AP lahendeid TSP lahendamiseks ära kasutavad. Mõelge kärpimise kontekstis.
#Изучите проблему assignment (AP) и можно ли с помощью этого решения добавить обрезку эффективней. У этих 2 проблем несколько состовляющих и разные алгоритмы, которые AP решения TSP используются при решении. Продумать контекст обрезки.
Mõelge sellele kuidas koguda ja hoida efektiivselt otsingu keskel vahetulemusi, sh juba läbitud teekonda.
#Продумайте как эффективно скапливать и держать по средине поиска результаты, уже проделанный путь. 
Mõelge kas suudate ära kasutada andmete sümmeetrilisust sümmeetriliste ülesannete korral.
#Продумайте сможите ли вы использовать ассиметричные данные для ассиметричного задания.
Enne põhialgoritmi käivitamist on mõistlik leida mõne ahne algoritmiga esimene suhteliselt hea lahendus, et kärpimine kohe algusest efektiivselt tööle hakkaks.
#Прежде чем приступить к основному алгоритму, разумно найти первое относительно хорошее решение с каким-то гениальным алгоритмом, чтобы работа была эффективней.
Erinevad kriteeriumid järgneva linna valimiseks. Järgmiste linnade sorteerimine sügavuti otsingul.
#Различные критерии выбора следующего города. Сортировка следующих городов при поиске по глубине.
Minimaalse katva puu (minimal spanning tree) algoritmide kasutamine.
#Минимальное покрытие (minimal spanning tree) использование алгоритма.